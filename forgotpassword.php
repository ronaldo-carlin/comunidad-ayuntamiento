<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>¿Has olvidado tu contraseña?</title>
    <!-- Favicon-->
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="assets/css/app.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/pages/extra_pages.css" rel="stylesheet" />
</head>

<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form id="email_confirm" class="login100-form validate-form" action="./?action=forgotpassword" enctype="multipart/form-data" method="POST">
                    <span class="login100-form-title p-b-45">
                        Restablecer la contraseña
                    </span>

                    <span class="error-subheader2 p-t-20 p-b-15">
                        Introduzca su dirección de correo electrónico registrada.
                    </span>
                    <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                        <input class="input100" name="email" type="email" id="email" required>
                        <span class="focus-input100"></span>
                        <span class="label-input100">Email</span>
                    </div>

                    <div class="container-login100-form-btn p-t-30">
                        <button class="login100-form-btn">
                            Restablecer mi contraseña
                        </button>
                    </div>

                    <div class="w-full p-t-25 text-center">

                        <div>
                            <a href="sign-in.html" class="txt1">
                                Iniciar sesión?
                            </a>
                        </div>
                    </div>

                </form>

                <div class="login100-more" style="background-image: url('assets/images/pages/bg-02.png');">
                </div>
            </div>
        </div>
    </div>

    <!-- Plugins Js -->
    <script src="assets/js/app.min.js"></script>
    <!-- Extra page Js -->
    <script src="assets/js/pages/examples/pages.js"></script>

    <script src="assets/js/form.min.js"></script>
    <script src="assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- Custom Js -->
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/forms/advanced-form-elements.js"></script>

    <script>
        $(function() {
            $("#email_confirm").submit(function(event) {
                event.preventDefault();

                var action = $(this).prop("action");
                var method = $(this).prop("method");
                var data = $(this).serialize();
                $.ajax({
                    method: method,
                    url: action,
                    data: data,
                }).done((data) => {
                    swal("Revisa tubandeja de entrada");
                    setTimeout(function() { // Delay para que se muestre el alert
                        window.location = 'index.php?view=home';
                    }, 1000);
                });

            });
        });
    </script>
</body>

</html>