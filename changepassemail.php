<?php

/* $sizeId = strlen($_GET["id"]) - 32;
$id = substr($_GET["id"], 16, $sizeId);
$token = substr($_GET["id"], $sizeId); */

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Actualización de contraseña</title>
    <!-- Favicon-->
    <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="assets/css/app.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/pages/extra_pages.css" rel="stylesheet" />
</head>

<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">


                <form id="update_pass" class="login100-form validate-form" action="./?action=updatepassword" enctype="multipart/form-data" method="POST">

                    <div class="login100-form-logo">
                        <div class="image">
                            <img src="assets/images/usrbig.jpg" alt="User">
                        </div>
                    </div>
                    <span class="login100-form-title p-b-34 p-t-27">
                        Emily Smith
                    </span>
                    <div class="text-center">
                        <p class="txt1 p-b-20">
                            Actualizar Contraseña
                        </p>
                    </div>


                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <!-- <input class="input100" type="password" name="pass"> -->
                        <input class="input100" name="newpass" type="password" id="password" required>
                        <span class="focus-input100"></span>
                        <span class="label-input100">Nueva Contraseña*</span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <input class="input100" name="password" id="confirmpassword" type="password" required>
                        <span class="focus-input100"></span>
                        <span class="label-input100">Confirmar Contraseña*</span>
                    </div>

                    <input class="form-control" name="type" hidden value="2">
                    <input class="form-control" name="id" hidden value="<?php echo $id ?>">

                    <div class="container-login100-form-btn p-t-30">
                        <button class="login100-form-btn">
                            Actualizar
                        </button>
                    </div>

                    <!-- <div class="w-full p-t-15 p-b-15 text-center">

                        <div>
                            <a href="#" class="txt1">
                                Need Help?
                            </a>
                        </div>
                    </div> -->

                </form>

                <div class="login100-more" style="background-image: url('assets/images/pages/bg-01.png');">
                </div>
            </div>
        </div>
    </div>

    <!-- Plugins Js -->
    <script src="assets/js/app.min.js"></script>
    <!-- Extra page Js -->
    <script src="assets/js/pages/examples/pages.js"></script>
    <script src="assets/js/form.min.js"></script>
    <script src="assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/forms/advanced-form-elements.js"></script>
    <script>
        $(function() {
            $("#update_pass").submit(function(event) {
                event.preventDefault();

                var password = document.getElementById("password").value;
                var confirmPassword = document.getElementById("confirmpassword").value;

                if (password != confirmPassword) {
                    swal({
                            title: "Las contraseñas no coiciden",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55"
                        },

                    );
                } else {
                    var form = this;
                    var formData = new FormData(form);
                    var action = $(this).prop("action");
                    var method = $(this).prop("method");
                    var data = $(this).serialize();

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        cache: false, //prueba
                        contentType: false, //prueba
                        processData: false, //prueba
                        data: formData,
                        //method: method,
                        url: action,
                    }).always((data) => {
                        swal("Actualizado Exitosamente", ":)", "success");
                        window.location = 'index.php?view=home';
                    });

                }


            });
        });
    </script>
</body>

</html>