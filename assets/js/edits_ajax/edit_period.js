//consulta para rellenar la tabla de periodos de la seccion
function periodo_id(btn) {
  let id = $(btn).data("section_id");
  $("#id_section_add").val(id);

  console.log(id);

  $data = {
    opt: "period",
    id: id,
  };

  /* consulta para tabla de secciones */
  envioAjax($data, "./index.php?action=ajaxrequest")
    .done((response) => {
      var data = response["sections"];
      let table = document.querySelector("#tab_period");

      table.innerHTML = "";

      table.innerHTML += `
                <tr class="text-center">
                    <td></td>
                    <td>NO SE ECONTRARON PERIODOS EN ESTA SECCION</td>
                    <td></td>
                </tr>
                `;
      //console.log(data);
      //console.log(response.sections[0].name);

      if (data.length > 0) {
        table.innerHTML = "";

        for (let item of data) {
          console.log(item.id);
          table.innerHTML += `
                <tr>
                    <td>${item.name}</td> 
                    <td>${item.description}</td>
                    <td>
                      <button class="btn tblActnBtn" id="btn_edit_section" onclick="edit_period(this)" data-section_id="${item.id}" data-toggle="modal" data-target="#edit_periodo">
                        <i class="icon-note blue-text"></i>
                      </button>

                      <button class="btn tblActnBtn">
                        <a href="index.php?action=delsection&id=${item.id}" class="delete">
                          <i class="icon-trash red-text"></i>
                        </a>
                      </button>

                      <button class="btn tblActnBtn" id="btn_seccion" onclick="element_id(this)" data-section_id="${item.id}" data-toggle="modal" data-target="#elemento">
                        <i class="icon-folder orange-text"></i>
                      </button>
                    </td>
                </tr>
                `;
        }
      }
    })
    .fail((jqXHR) => {
      console.log(jqXHR);
    });
}

//consulta para hacer el update del periodo
function edit_period(btn) {
  let id = $(btn).data("section_id");
  $("#id_section_edit").val(id);

  console.log(id);

  $data = {
    opt: "period_id",
    id: id,
  };

  envioAjax($data, "./index.php?action=ajaxrequest")
    .done((response) => {
      //console.log(response.section[0].description);

      $("#period_name_edit").val(response.section[0].name);
      
      $("#des_edit_period").val(response.section[0].description);
    })
    .fail((jqXHR) => {
      console.log(jqXHR);
    });
}

//mensaje cuando se hace un update de la periodo
$(function () {
  $("#updateperiod").submit(function (event) {
    event.preventDefault();

    var form = this;
    var formData = new FormData(form);
    console.log(formData);
    var id = $("#id_fraction").val();
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();

    // data += "&id=" + id + "&type=" + type;
    //console.log(data)
    $.ajax({
      type: "POST",
      dataType: "json",
      cache: false, //prueba
      contentType: false, //prueba
      processData: false, //prueba
      data: formData,
      //method: method,
      url: action,
    }).always((data) => {
      swal("Actualizado Exitosamente", ":)", "success");
      $("#edit_periodo").modal("hide");
      setTimeout(function () {
        // Delay para que se muestre el alert
        location.reload();
      }, 500);
    });
  });
});
