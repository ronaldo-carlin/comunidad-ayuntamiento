var edit;
ClassicEditor.create(document.querySelector("#description_edit"))
  .then((editor) => {
    edit = editor;
  })
  .catch((error) => {
    console.error(error);
  });
function quality_id(btn) {
  var id = $(btn).data("quality_id");
  //console.log(id);

  $.ajax({
    method: "POST",
    url: "index.php?action=ajaxrequest",
    data: {
      id: id,
      opt: "quality",
    },
  }).done(function (data) {
    console.log(data);

    $("#name_quality").val(data["quality"][0].name);
    edit.setData(data["quality"][0].description);
    edit.updateSourceElement();
    edit.model.document.on("change:data", () => {
      var val = edit.getData();
      $("#description_edit").val(val);
      console.log(val);
    });
    $("#id").val(data["quality"][0].id);

    $("#link_quality").val(data["quality"][0].link);
    if (data["quality"][0].publish == 1) {
      $("#publish_quality").prop("checked", true);
      $("#publish_quality").val(data["quality"][0].publish);
    }
    $("#date_quality").val(data["quality"][0].date_validity);
    $("#type_quality").val(data["quality"][0].type_id);
    /* $.each(data["quality"][0].companies_avalibles.split(","), function (i, e) {
      $("#companies_avalibles option[value='" + e + "']").prop(
        "selected",
        true
      );
    }); */
  });
}
$(function () {
  $("#updatequality").submit(function (event) {
    event.preventDefault();

    var form = this;
    var formData = new FormData(form);
    //console.log(formData);
    var id = $("#id").val();
    var type = $("#type").val();
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();

    // data += "&id=" + id + "&type=" + type;
    //console.log(data)
    $.ajax({
      type: "POST",
      dataType: "json",
      cache: false, //prueba
      contentType: false, //prueba
      processData: false, //prueba
      data: formData,
      //method: method,
      url: action,
    }).always((data) => {
      swal("Actualizado Exitosamente", ":)", "success");
      $("#edit_qualitys").modal("hide");
      setTimeout(function () {
        // Delay para que se muestre el alert
        location.reload();
      }, 1000);
    });
  });
});
