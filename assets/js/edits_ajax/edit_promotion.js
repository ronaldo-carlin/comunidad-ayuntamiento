var edit;
ClassicEditor.create(document.querySelector("#description_edit"))
  .then((editor) => {
    edit = editor;
  })
  .catch((error) => {
    console.error(error);
  });

function promotion_id(btn) {
  var id = $(btn).data("promotion_id");
  //console.log(id);

  $.ajax({
    method: "POST",
    url: "index.php?action=ajaxrequest",
    data: {
      id: id,
      opt: "promotions",
    },
  }).done(function (data) {
    console.log(data);
    $("#name_promotion").val(data["promotions"][0].name);
    
    edit.setData(data["promotions"][0].description);
    edit.updateSourceElement();
    edit.model.document.on("change:data", () => {
      var val = edit.getData();
      $("#description_edit").val(val);
      console.log(val);
    });
    $("#company_promotion").val(data["promotions"][0].company_id);
    $("#date_promotion").val(data["promotions"][0].date_validity);
    $("#id").val(data["promotions"][0].id);
    $.each(
      data["promotions"][0].companies_avalibles.split(","),
      function (i, e) {
        $("#companies_avalibles option[value='" + e + "']").prop(
          "selected",
          true
        );
      }
    );
    if (data["promotions"][0].publish == 1) {
      $("#publish_promotion").prop("checked", true);
      $("#publish_promotion").val(data["promotions"][0].publish);
    }
  });
}
$(function () {
  $("#updatepromotion").submit(function (event) {
    event.preventDefault();
    var form = this;
    var formData = new FormData(form);
    //console.log(formData);
    var id = $("#id").val();
    var type = $("#type").val();
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();

    // data += "&id=" + id + "&type=" + type;
    //console.log(data)
    $.ajax({
      type: "POST",
      dataType: "json",
      cache: false, //prueba
      contentType: false, //prueba
      processData: false, //prueba
      data: formData,
      //method: method,
      url: action,
    }).always((data) => {
       swal("Actualizado Exitosamente", ":)", "success");
      $("#edit_promotion").modal("hide");
      setTimeout(function () {
        // Delay para que se muestre el alert
        location.reload();
      }, 1000);
    });
  });
});
