function user_id(btn) {
  var id = $(btn).data("user_id");
  console.log(id);

  $.ajax({
    method: "POST",
    url: "index.php?action=ajaxrequest",
    data: {
      user_id: id,
      opt: "update_user",
    },
  }).done(function (data) {
    //console.log(data);
    $("#name_user_edit").val(data["user"][0].name);
    $("#email_user").val(data["user"][0].email);
    $("#password_user").val(data["user"][0].password);
    $("#date_birth_user").val(data["user"][0].date_birth);
    $("#phone_user").val(data["user"][0].phone);
    $("#address_user").val(data["user"][0].address);
    $("#curp_user").val(data["user"][0].curp);
    $("#rfc_user").val(data["user"][0].rfc);
    $("#imss_user").val(data["user"][0].imss);
    $("#date_admission_user").val(data["user"][0].date_admission);
    $("#id").val(data["user"][0].id);

    $("#department_user option")
      .removeAttr("selected")
      .filter("[value='" + data["user"][0].department_id + "']")
      .attr("selected", true);
    $("#area_user option")
      .removeAttr("selected")
      .filter("[value='" + data["user"][0].area_id + "']")
      .attr("selected", true);
    $("#role_id_user option")
      .removeAttr("selected")
      .filter("[value='" + data["user"][0].rol_id + "']")
      .attr("selected", true);
  });
}

$(function () {
  $("#update_user").submit(function (event) {
    event.preventDefault();

    var form = this;
    var formData = new FormData(form);
    var id = $("#id").val();
    //console.log(id);
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();
    //data += "&id=" + id;
    //console.log(data);
    $.ajax({
      type: "POST",
      dataType: "json",
      cache: false, //prueba
      contentType: false, //prueba
      processData: false, //prueba
      data: formData,
      //method: method,
      url: action,
    }).always((data) => {
      swal("Actualizado Exitosamente", ":)", "success");
      $("#editUser").modal("hide");

      setTimeout(function () {
        // Delay para que se muestre el alert
        //location.reload();
      }, 1000);
    });
  });
});
