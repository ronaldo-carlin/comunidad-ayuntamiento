var edit;
ClassicEditor.create(document.querySelector("#description_edit"))
  .then((editor) => {
    edit = editor;
  })
  .catch((error) => {
    console.error(error);
  });

function release_id(btn) {
  var id = $(btn).data("release_id");
  var role_id = $(btn).data("role_id");
  var api_token = $(btn).data("api_token");


  $.ajax({
    method: "POST",
    url: "index.php?action=ajaxrequest",
    data: {
      id: id,
      role_id: role_id,
      api_token: api_token,
      opt: "releases",
    },
  }).done(function (data) {
    console.log(data);

    $("#name_release").val(data["releases"][0].name);
    edit.setData(data["releases"][0].description);
    edit.updateSourceElement();
    edit.model.document.on("change:data", () => {
      var val = edit.getData();
      $("#description_edit").val(val);
      console.log(val);
    });
    /*  CKEDITOR.instances.editor1.focus();
    CKEDITOR.instances["editor1"].setData(data["releases"][0].description);
    CKEDITOR.instances["editor1"].updateElement(); */

    $("#date_release").val(data["releases"][0].date_validity);
    $("#company_release").val(data["releases"][0].company_id);
    $("#id").val(data["releases"][0].id);

    $.each(data["releases"][0].companies_avalibles.split(","), function (i, e) {
      $("#companies_avalibles option[value='" + e + "']").prop(
        "selected",
        true
      );
    });
    if (data["releases"][0].publish == 1) {
      $("#publish_release").prop("checked", true);
      $("#publish_release").val(data["releases"][0].publish);
    }
  });
}
$(function () {
  $("#updaterelease").submit(function (event) {
    event.preventDefault();
    var form = this;
    var formData = new FormData(form);
    var id = $("#id").val();
    //console.log(id);
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();
    //data += "&id=" + id;
    //console.log(data);
    $.ajax({
      type: "POST",
      dataType: "json",
      cache: false, //prueba
      contentType: false, //prueba
      processData: false, //prueba
      data: formData,
      //method: method,
      url: action,
    }).always((data) => {
       swal("Actualizado Exitosamente", ":)", "success")
       $("#edit_release").modal('hide')
      
        setTimeout(function() { // Delay para que se muestre el alert
            location.reload();
        }, 1000);  
    });
  });
});
