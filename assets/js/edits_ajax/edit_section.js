//consulta para rellenar la tabla de secciones de la fraccion
function section_id(btn) {
  let id = $(btn).data("fraction_id");

  //le paso el id de la fraccion para cuando quieran agregar una nueva seccion de esta fraccion
  $("#id_frac_sec").val(id);

  console.log(id);

  $data = {
    opt: "sections",
    id: id,
  };

  /* consulta para tabla de secciones */
  envioAjax($data, "./index.php?action=ajaxrequest")
    .done((response) => {
      var data = response["sections"];
      let table = document.querySelector("#tab_section");

      table.innerHTML = "";

      table.innerHTML += `
                <tr class="text-center">
                    <td></td>
                    <td>NO SE ECONTRARON SECCIONES</td>
                    <td></td>
                </tr>
                `;

      if (data.length > 0) {
        table.innerHTML = "";

        for (let item of data) {
          console.log(item.id);
          table.innerHTML += `
                <tr>
                    <td>${item.name}</td> 
                    <td>${item.description}</td>
                    <td>
                      <button class="btn tblActnBtn" id="btn_edit_section" onclick="edit_section(this)" data-section_id="${item.id}" data-toggle="modal" data-target="#edit_section">
                        <i class="icon-note blue-text"></i>
                      </button>

                      <button class="btn tblActnBtn">
                        <a href="index.php?action=delsection&id=${item.id}" class="delete">
                          <i class="icon-trash red-text"></i>
                        </a>
                      </button>

                      <button class="btn tblActnBtn" id="btn_seccion" onclick="periodo_id(this)" data-section_id="${item.id}" data-toggle="modal" data-target="#periodo">
                        <i class="icon-list orange-text"></i>
                      </button>
                    </td>
                </tr>
                `;
        }
      }
    })
    .fail((jqXHR) => {
      console.log(jqXHR);
    });
}

//consulta para hacer el update de la seccion
function edit_section(btn) {
  let id = $(btn).data("section_id");
  $("#section_id").val(id);

  console.log(id);

  $data = {
    opt: "sections_id",
    id: id,
  };

  envioAjax($data, "./index.php?action=ajaxrequest")
    .done((response) => {
      //console.log(response.section[0].description);

      $("#sel_edit_section option")
        .removeAttr("selected")
        .filter("[value='" + response.section[0].name + "']")
        .attr("selected", true);

      $("#des_edit_section").val(response.section[0].description);
    })
    .fail((jqXHR) => {
      console.log(jqXHR);
    });
}

//mensaje cuando se hace un update de la seccion
$(function () {
  $("#updatesection").submit(function (event) {
    event.preventDefault();

    var form = this;
    var formData = new FormData(form);
    console.log(formData);
    var id = $("#id_fraction").val();
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();

    // data += "&id=" + id + "&type=" + type;
    //console.log(data)
    $.ajax({
      type: "POST",
      dataType: "json",
      cache: false, //prueba
      contentType: false, //prueba
      processData: false, //prueba
      data: formData,
      //method: method,
      url: action,
    }).always((data) => {
      swal("Actualizado Exitosamente", ":)", "success");
      $("#edit_fraction").modal("hide");
      setTimeout(function () {
        // Delay para que se muestre el alert
        location.reload();
      }, 500);
    });
  });
});

function envioAjax(datas = [], url) {
  var jqXHR = $.ajax({
    url: url,
    dataType: "JSON",
    type: "POST",
    data: datas,
  });

  return jqXHR;
}
