function event_id(btn) {
  var id = $(btn).data("event_id");
  //console.log(id);

  $.ajax({
    method: "POST",
    url: "index.php?action=ajaxrequest",
    data: {
      id: id,
      opt: "events",
    },
  }).done(function (data) {
    //console.log(data);
    $("#name_event").val(data['events'][0].name);
    $("#type_event").val(data['events'][0].event_id);
    $("#company_event").val(data['events'][0].company_id);
    $.each(data['events'][0].companies_avalibles.split(","), function (i, e) {
      $("#companies_avalibles option[value='" + e + "']").prop(
        "selected",
        true
      );
    });
    if (data['events'][0].repeat_yearly == 1) {
      $("#repeat_yearly_event").prop("checked", true);
      $("#repeat_yearly_event").val(data['events'][0].repeat_yearly);
    }

    $("#date_start_event").val(data['events'][0].date_start);
    $("#date_end_event").val(data['events'][0].date_end);
  });
}
$(function () {
  $("#updateevent").submit(function (event) {
    event.preventDefault();

    var id = $("#event_id").data("event_id");
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();
    data += "&id=" + id;
    //console.log(data)
    $.ajax({
      method: method,
      url: action,
      data: data,
    }).done((data) => {
       swal("Actualizado Exitosamente", ":)", "success");
       $("#edit_event").modal("hide");
       setTimeout(function () {
         // Delay para que se muestre el alert
         location.reload();
       }, 1000);
    });
  });
});
