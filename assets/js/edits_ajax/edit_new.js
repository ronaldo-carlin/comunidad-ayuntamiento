var news_editor;
ClassicEditor.create(document.querySelector("#description_editt"))
  .then((editor) => {
    news_editor = editor;
  })
  .catch((error) => {
    console.error(error);
  });

ClassicEditor.create(document.querySelector("#editor"))
  .catch((error) => {
  console.error(error);
});

var new_web_editor;
ClassicEditor.create(document.querySelector("#description_web_edit"))
  .then((editor) => {
    new_web_editor = editor;
  })
  .catch((error) => {
    console.error(error);
  });

function news_id(btn) {
  var id = $(btn).data("news_id");
  console.log(id);

  $.ajax({
    method: "POST",
    url: "./index.php?action=ajaxrequest",
    data: {
      id: id,
      opt: "news",
    },
  }).done(function (data) {
    console.log(data);
    console.log(news_editor);
    var type = $("#type").val();
    //! Noticias ad,om
    $("#title_edit").val(data["news"][0].title);

    $("#id").val(data["news"][0].id);

    $("#link_edit").val(data["news"][0].link);
    $("#link_youtube_edit").val(data["news"][0].link_youtube);
    if (data["news"][0].publish == 1) {
      $("#publish_edit").prop("checked", true);
      $("#publish_edit").val(data["news"][0].publish);
    }
    $("#date_validity_edit").val(data["news"][0].date_validity);

    $("#departments_edit").append(
      "<option value='' disabled selected>" +
        data["news"][0].departments +
        "</option>"
    );

    news_editor.updateSourceElement();
    news_editor.setData(data["news"][0].description);

  });
}

$(function () {
  $("#updatenew").submit(function (event) {
    event.preventDefault();

    var form = this;
    var formData = new FormData(form);
    //console.log(formData);
    var id = $("#id").val();
    var type = $("#type").val();
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();

    // data += "&id=" + id + "&type=" + type;
    //console.log(data)
    $.ajax({
      type: "POST",
      dataType: "json",
      cache: false, //prueba
      contentType: false, //prueba
      processData: false, //prueba
      data: formData,
      //method: method,
      url: action,
    }).always((data) => {
      swal("Actualizado Exitosamente", ":)", "success");
      $("#edit_news").modal("hide");
      setTimeout(function () {
        // Delay para que se muestre el alert
        /* location.reload(); */
      }, 1000);
    });
  });
});
