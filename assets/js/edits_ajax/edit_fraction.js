var description_edit;
ClassicEditor.create(document.querySelector("#description_edit"))
  .then((editor) => {
    description_edit = editor;
  })
  .catch((error) => {
    console.error(error);
  });

//consulta para rellenar campos
function fraction_id(btn) {
  var id = $(btn).data("fraction_id");

  $("#id_fraction").val(id);

  console.log(id);

  $.ajax({
    method: "POST",
    url: "./index.php?action=ajaxrequest",
    data: {
      id: id,
      opt: "fraction",
    },
  }).done(function (data) {
    console.log(data);
    /* var type = $("#type").val(); */

    $("#fraction_edit").val(data["fraction"][0].fraction);

    $("#name_user_edit").val(data["fraction"][0].name);

    $("#description_edit").val(data["fraction"][0].description);
    description_edit.updateSourceElement();
    description_edit.setData(data["fraction"][0].description);

    $("#format_fraction_edit").val(data["fraction"][0].format);

    $("#update_date_edit").val(data["fraction"][0].update_date);

    $("#date_validity_edit").val(data["fraction"][0].date_validation);

    $("#department_edit option")
      .removeAttr("selected")
      .filter("[value='" + data["fraction"][0].department_id + "']")
      .attr("selected", true);

    $("#type_edit option")
      .removeAttr("selected")
      .filter("[value='" + data["fraction"][0].type_id + "']")
      .attr("selected", true);

    $("#update_period_edit option")
      .removeAttr("selected")
      .filter("[value='" + data["fraction"][0].update_period + "']")
      .attr("selected", true);
  });
}

//mensaje y accion cuando se enviar formulario
$(function () {
  $("#updatefraction").submit(function (event) {
    event.preventDefault();

    var form = this;
    var formData = new FormData(form);
    console.log(formData);
    var id = $("#id_fraction").val();
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();

    // data += "&id=" + id + "&type=" + type;
    //console.log(data)
    $.ajax({
      type: "POST",
      dataType: "json",
      cache: false, //prueba
      contentType: false, //prueba
      processData: false, //prueba
      data: formData,
      //method: method,
      url: action,
    }).always((data) => {
      swal("Actualizado Exitosamente", ":)", "success");
      $("#edit_fraction").modal("hide");
      setTimeout(function () {
        // Delay para que se muestre el alert
        location.reload();
      }, 500);
    });
  });
});
