//consulta para rellenar la tabla de elementos de la seccion
function element_id(btn) {
  let id = $(btn).data("section_id");
  $("#id_element_add").val(id);

  console.log(id);

  $data = {
    opt: "element",
    id: id,
  };

  /* consulta para tabla de secciones */
  envioAjax($data, "./index.php?action=ajaxrequest")
    .done((response) => {
      var data = response["elements"];
      let table = document.querySelector("#tab_element");

      //console.log(response.elements[0].link);
      //console.log(data);

      if (data.length > 0) {

        table.innerHTML = "";

        for (let item of data) {
          console.log(item.id);
          table.innerHTML += `
                <tr>
                    <td style="width: 45%">
                      <a href="${item.link}" target="_blank" class="">
                        ${item.link}
                      </a>
                    </td>

                    <td>${item.description}</td>

                    <td>
                      <button class="btn tblActnBtn" id="btn_edit_section" onclick="edit_element(this)" data-period_id="${item.id}" data-toggle="modal" data-target="#edit_element">
                        <i class="icon-note blue-text"></i>
                      </button>

                      <button class="btn tblActnBtn">
                        <a href="index.php?action=delelement&id=${item.id}" class="delete">
                          <i class="icon-trash red-text"></i>
                        </a>
                      </button>
                    </td>
                </tr>
                `;
        }
      } else {
        table.innerHTML = "";
        
        table.innerHTML += `
                <tr class="text-center">
                    <td></td>
                    <td>NO SE ECONTRARON ELEMENTOS EN ESTE PERIODO</td>
                    <td></td>
                </tr>
                `;
      }
    })
    .fail((jqXHR) => {
      console.log(jqXHR);
    });
}

//consulta para hacer el update al elemento
function edit_element(btn) {
  let id = $(btn).data("period_id");
  $("#id_element_edit").val(id);

  console.log(id);

  $data = {
    opt: "element_id",
    id: id,
  };

  envioAjax($data, "./index.php?action=ajaxrequest")
    .done((response) => {
      $("#link_edit_element").val(response.elements[0].link);

      $("#des_edit_element").val(response.elements[0].description);
    })
    .fail((jqXHR) => {
      console.log(jqXHR);
    });
}

//mensaje cuando se hace unpdate de la seccion
$(function () {
  $("#updateelement").submit(function (event) {
    event.preventDefault();

    var form = this;
    var formData = new FormData(form);
    console.log(formData);
    var id = $("#id_fraction").val();
    var action = $(this).prop("action");
    var method = $(this).prop("method");
    var data = $(this).serialize();

    // data += "&id=" + id + "&type=" + type;
    //console.log(data)
    $.ajax({
      type: "POST",
      dataType: "json",
      cache: false, //prueba
      contentType: false, //prueba
      processData: false, //prueba
      data: formData,
      //method: method,
      url: action,
    }).always((data) => {
      swal("Actualizado Exitosamente", ":)", "success");
      $("#edit_fraction").modal("hide");
      setTimeout(function () {
        // Delay para que se muestre el alert
        location.reload();
      }, 500);
    });
  });
});
