<?php
$permissions = UserData::getPermissonsByUserAndModule($_SESSION["user_id"], 5);

if ($permissions->read == 1) :

    $u = UserData::getUserByIdAPI($_SESSION["user_id"]);
    $departments = DepartmentsData::getDepartments();
?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">Aviso de Privacidad</h4>
                            </li>
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="./index.php?view=home">
                                    <i class="fas fa-home"></i> Inicio</a>
                            </li>
                            <li class="breadcrumb-item active">Aviso de Privacidad</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Your content goes here  -->
        </div>
    </section>

<?php else : Core::redir("./index.php?view=home");
endif ?>