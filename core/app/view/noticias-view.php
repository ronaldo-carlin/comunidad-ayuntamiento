<?php
$u = UserData::getById($_SESSION["user_id"]);
$rol = RolesData::getActivesAPI();

$news = NewsData::getAllNews();
$departments = DepartmentsData::getDepartments();

/* $newsAllCount = NewsData::getAllCount()->count;
$newsPublishCount = NewsData::getPublishCount()->count;
$newsNoPublishCount = NewsData::getNopublishCount()->count;

$percentAll = ($newsAllCount * 100) / $newsAllCount;
$percentPublish = ($newsPublishCount * 100) / $newsAllCount;
$percentNoPublish = ($newsNoPublishCount * 100) / $newsAllCount; */
?>

<section class="content">
    <div class="container-fluid">

        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Noticias</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="./index.php?view=home">
                                <i class="fas fa-home"></i> Inicio</a>
                        </li>
                        <li class="breadcrumb-item active">Noticias</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="bootstrap snippet">
                            <section id="portfolio" class="gray-bg padding-top-bottom">
                                <!--==== Portfolio Filters ====-->
                                <!-- <div class="categories">
                                    <ul>
                                        <li class="active">
                                            <a href="#" data-filter="*">All Categories</a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".web-design">Web Design</a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".apps">Apps</a>
                                        </li>
                                        <li>
                                            <a href="#" data-filter=".psd">PSD</a>
                                        </li>
                                    </ul>
                                </div> -->

                                <!-- ======= Portfolio items ===-->
                                <div class="projects-container scrollimation in">
                                    <div class="row">
                                        <?php $count = 0;
                                        foreach ($news['news'] as $n) :
                                            $count++ ?>
                                            <tr>
                                                <article class="col-md-4 col-sm-6 portfolio-item web-design apps psd">
                                                    <div class="portfolio-thumb in">
                                                        <a href="#" class="main-link">
                                                            <!-- <img class="img-responsive img-center" src="assets/images/posts/post4.jpg" alt=""> -->
                                                            <img class="img-responsive img-center w-100" src="files/imgNews/<?php echo $n['cover_page']; ?>" alt="">
                                                            <span class="project-title"><?php echo $n['title']; ?></span>
                                                            <span class="overlay-mask"></span>
                                                        </a>
                                                        <!-- <a class="enlarge cboxElement" href="#" title="Bills Project"><i class="fa fa-expand fa-fw"></i></a> -->
                                                        <a class="link centered" href="#"><i class="fa fa-eye fa-fw"></i></a>
                                                    </div>
                                                </article>
                                            <?php endforeach; ?>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Browser Usage -->
        </div>
    </div>
</section>

<!-- Custom Js -->
<script src="assets/js/pages/apps/portfolio.js"></script>