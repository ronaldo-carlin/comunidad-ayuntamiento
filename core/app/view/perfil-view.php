<?php
$u = UserData::getUserByIdAPI($_SESSION["user_id"]);
foreach ($u['user'] as $u) :
?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">Perfi</h4>
                            </li>
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="index.php?view=home">
                                    <i class="fas fa-home"></i>Inicio</a>
                            </li>

                            <li class="breadcrumb-item active">Perfil</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Your content goes here  -->
            <div class="row clearfix">
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="m-b-20">
                            <div class="contact-grid">
                                <div class="profile-header bg-dark">
                                    <div class="user-name"><?php echo $u['name']; ?></div>
                                    <div class="name-center">Departamento: <?php echo $u['departments']; ?></div>
                                    <!-- <div class="name-center">Puesto: <?php echo $u['area']; ?></div> -->
                                </div>
                                <?php if ($u['img'] == '' || $u['img'] == NULL) : ?>
                                    <img src="assets/images/user/user.png" class="user-img">
                                <?php else : ?>
                                    <img src="files/imgUsers/<?php echo $u['img'] ?>" class="user-img">
                                <?php endif; ?>

                                <?php if ($u['address'] == '' || $u['address'] == NULL) : ?>
                                    <p>sin dirección</p>
                                <?php else : ?>
                                    <p><?php echo $u['address']; ?></p>
                                <?php endif; ?>

                                <div>
                                    <span class="phone">
                                        <i class="material-icons">phone</i><?php echo $u['phone']; ?></span>
                                </div>

                                <!-- <div class="row">
                                    <div class="col-4">
                                        <h5>564</h5>
                                        <small>Following</small>
                                    </div>
                                    <div class="col-4">
                                        <h5>18k</h5>
                                        <small>Followers</small>
                                    </div>
                                    <div class="col-4">
                                        <h5>565</h5>
                                        <small>Post</small>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <!-- <div class="card">
                        <ul class="nav nav-tabs">
                            <li class="nav-item m-l-10">
                                <a class="nav-link active" data-toggle="tab" href="#about">About</a>
                            </li>
                            <li class="nav-item m-l-10">
                                <a class="nav-link" data-toggle="tab" href="#skills">Skills</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane body active" id="about">
                                <p class="text-default">Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has
                                    been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer
                                    took a galley of type and scrambled it to make a type specimen book. It has
                                    survived
                                    not only five centuries, but also the leap into electronic typesetting, remaining
                                    essentially
                                    unchanged.</p>
                                <small class="text-muted">Email address: </small>
                                <p>john@gmail.com</p>
                                <hr>
                                <small class="text-muted">Phone: </small>
                                <p>+91 1234567890</p>
                                <hr>
                            </div>
                            <div class="tab-pane body" id="skills">
                                <ul class="list-unstyled">
                                    <li>
                                        <div>Photoshop</div>
                                        <div class="progress skill-progress m-t-20">
                                            <div class="progress-bar l-bg-green width-per-45" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>Wordpress</div>
                                        <div class="progress skill-progress m-b-20">
                                            <div class="progress-bar l-bg-orange width-per-38" role="progressbar" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>HTML 5</div>
                                        <div class="progress skill-progress m-b-20">
                                            <div class="progress-bar l-bg-cyan width-per-39" role="progressbar" aria-valuenow="39" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div>Angular</div>
                                        <div class="progress skill-progress m-b-20">
                                            <div class="progress-bar l-bg-purple width-per-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->

                </div>
                <div class="col-lg-8 col-md-12">

                    <div class="card">
                        <div class="profile-tab-box">
                            <div class="p-l-20">
                                <ul class="nav ">
                                    <li class="nav-item tab-all">
                                        <a class="nav-link active show" href="#project" data-toggle="tab">Sobre Mí</a>
                                    </li>
                                    <li class="nav-item tab-all p-l-20">
                                        <a class="nav-link" href="#usersettings" data-toggle="tab">Configuración</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="project" aria-expanded="true">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card project_widget">
                                        <div class="header">
                                            <h2>About</h2>
                                        </div>
                                        <div class="body">
                                            <div class="row">
                                                <div class="col-md-3 col-6 b-r">
                                                    <strong>Full Name</strong>
                                                    <br>
                                                    <p class="text-muted">Emily Smith</p>
                                                </div>
                                                <div class="col-md-3 col-6 b-r">
                                                    <strong>Mobile</strong>
                                                    <br>
                                                    <p class="text-muted">(123) 456 7890</p>
                                                </div>
                                                <div class="col-md-3 col-6 b-r">
                                                    <strong>Email</strong>
                                                    <br>
                                                    <p class="text-muted">johndeo@example.com</p>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <strong>Location</strong>
                                                    <br>
                                                    <p class="text-muted">India</p>
                                                </div>
                                            </div>
                                            <p class="m-t-30">Completed my graduation in Arts from the well known and
                                                renowned institution
                                                of India – SARDAR PATEL ARTS COLLEGE, BARODA in 2000-01, which was
                                                affiliated
                                                to M.S. University. I ranker in University exams from the same
                                                university
                                                from 1996-01.</p>
                                            <p>Worked as Professor and Head of the department at Sarda Collage, Rajkot,
                                                Gujarat
                                                from 2003-2015 </p>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem
                                                Ipsum has been the industry's standard dummy text ever since the 1500s,
                                                when
                                                an unknown printer took a galley of type and scrambled it to make a
                                                type
                                                specimen book. It has survived not only five centuries, but also the
                                                leap
                                                into electronic typesetting, remaining essentially unchanged.</p>
                                            <br>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card project_widget">
                                        <div class="header">
                                            <h2>Education</h2>
                                        </div>
                                        <div class="body">
                                            <ul>
                                                <li>B.A.,Gujarat University, Ahmedabad,India.</li>
                                                <li>M.A.,Gujarat University, Ahmedabad, India.</li>
                                                <li>P.H.D., Shaurashtra University, Rajkot</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card project_widget">
                                        <div class="header">
                                            <h2>Experience</h2>
                                        </div>
                                        <div class="body">
                                            <ul>
                                                <li>One year experience as Jr. Professor from April-2009 to march-2010
                                                    at B.
                                                    J. Arts College, Ahmedabad.</li>
                                                <li>Three year experience as Jr. Professor at V.S. Arts &amp; Commerse
                                                    Collage
                                                    from April - 2008 to April - 2011.</li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="card project_widget">
                                        <div class="header">
                                            <h2>Conferences, Cources &amp; Workshop Attended</h2>
                                        </div>
                                        <div class="body">
                                            <ul>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                                <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="timeline" aria-expanded="false">
                        </div>

                        <div role="tabpanel" class="tab-pane" id="usersettings" aria-expanded="false">

                            <div class="card">
                                <div class="header">
                                    <h2>
                                        <strong>Seguridad</strong> Configuración
                                    </h2>
                                </div>
                                <div class="body">
                                    <form id="form_validation" action="index.php?action=updatepassword" enctype="multipart/form-data" method="POST">

                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group form-float">
                                                <div class="form-line focused">
                                                    <input type="text" disabled name="email" class="form-control" placeholder="Correo Eletronico" value="<?php echo $u['email']; ?>">
                                                    <label class="form-label" style="color: #fff !important;">Correo eletronico</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 d-flex align-items-end">
                                            <!-- <input type="password" name="password" class="form-control" placeholder="Contraseña Actual" value="<?php echo $u['password']; ?>"> -->
                                            <div class="col-10 p-0">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <label class="form-label">Contraseña Actual</label>
                                                        <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña Actual" value="<?php echo $u['password']; ?>" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1 p-0">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <button class="form-control p-0" type="button" onclick="mostrarPasswordadd()">
                                                            <span id="icon1" class="fa fa-eye-slash icon" style="font-size: 20px;"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 d-flex align-items-end">
                                            <!-- <input type="password" name="password" class="form-control" placeholder="Contraseña Actual" value="<?php echo $u['password']; ?>"> -->
                                            <div class="col-10 p-0">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <label class="form-label">Contraseña Nueva</label>
                                                        <input type="password" class="form-control" name="newpass" id="newpass" placeholder="Contraseña Nueva" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1 p-0">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <button class="form-control p-0" type="button" onclick="mostrarPasswordnew()">
                                                            <span id="icon2" class="fa fa-eye-slash icon" style="font-size: 20px;"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="text" name="id" class="form-control" hidden value="<?php echo $u['id']; ?>">
                                        <button class="btn btn-info btn-round">Guardar Cambios</button>
                                    </form>
                                </div>
                            </div>

                            <div class="card">
                                <div class="header">
                                    <h2>
                                        <strong>Cuenta</strong> Configuración
                                    </h2>
                                </div>
                                <div class="body">
                                    <form id="form_validation" action="index.php?action=updateprofile" enctype="multipart/form-data" method="POST">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" hidden name="id" value="<?php echo $u['id']; ?>">
                                                    <input type="text" class="form-control" name="name" placeholder="Nombre Completo" value="<?php echo $u['name']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="email" placeholder="E-mail" value="<?php echo $u['email']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="phone" placeholder="Telefono" value="<?php echo $u['phone']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="curp" placeholder="Curp" value="<?php echo $u['curp']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea rows="1" class="form-control no-resize" name="address" placeholder="Dirección"><?php echo $u['address']; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group form-float">
                                                    <label class="">Imagen de perfil*</label>
                                                    <div class="form-line">
                                                        <input type="file" class="default" name="img">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button class="btn btn-primary btn-round">Guardar Cambios</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- <div class="card">
                                <div class="header">
                                    <h2>
                                        <strong>Account</strong> Settings
                                    </h2>
                                </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="City">
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="E-mail">
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Country">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea rows="4" class="form-control no-resize" placeholder="Address Line 1"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="form-check m-l-10">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" id="checkbox" name="checkbox"> Profile Visibility For Everyone
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check m-l-10">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" id="checkbox1" name="checkbox"> New task notifications
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check m-l-10">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" id="checkbox2" name="checkbox"> New friend request notifications
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button class="btn btn-primary btn-round">Save Changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        function mostrarPasswordadd() {
            var cambio = document.getElementById("password");
            if (cambio.type == "password") {
                cambio.type = "text";
                $('#icon1').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
            } else {
                cambio.type = "password";
                $('#icon1').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
            }
        }

        function mostrarPasswordnew() {
            var cambio = document.getElementById("newpass");
            if (cambio.type == "password") {
                cambio.type = "text";
                $('#icon2').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
            } else {
                cambio.type = "password";
                $('#icon2').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
            }
        }
    </script>

<?php endforeach; ?>