<?php
$permissions = UserData::getPermissonsByUserAndModule($_SESSION["user_id"], 16);

if ($permissions->read == 1) :

    $u = UserData::getById($_SESSION["user_id"]);
    $rol = RolesData::getActivesAPI();

    if ($_SESSION["rol_id"] == 1 || $_SESSION["rol_id"] == 5) {

        $fraction = FractionData::getAllByAdmin($_SESSION['administration_id']);
    } elseif ($_SESSION["rol_id"] == 10) {

        if ($_SESSION['department_id'] == 20) {
            $fraction = FractionData::getAllByAdmin($_SESSION['administration_id']);
        } else {
            $fraction = FractionData::getAllByAdminandDepartment($_SESSION['administration_id'], $_SESSION['department_id']);
            echo '<pre>';
            print_r($_SESSION['administration_id']);
            echo '<br>';
            print_r($_SESSION['department_id']);
            echo '</pre>';
        }
        
    } else {
        echo 'NO TIENES ACCESO A LAS FRACIONES';
    }

    $typefraction = FractionData::getTypeFraction();
    $period = FractionData::getAllPeriod();
    $departments = DepartmentsData::getDepartments();
    $administration = AdministrationData::getAllAdministration();

    /* filtrar fraciones por tipo de la administracion actual */

    foreach ($administration['administration'] as $a) {
        $id = $a['id'];
        $fecha_inicio = $a['date_start'];
        $fecha_final = $a['date_end'];

        $s = explode("-", $fecha_inicio);
        $e = explode("-", $fecha_final);

        $start = $s[0];
        $end = $e[0];

        $resultado = $end - $start;
    }

    $section = array();
    $contador = 0;
    for ($i = $start; $i <= $end; $i++) {
        /* echo '<pre>';
        print_r($i);
        echo '</pre>'; */

        $section[$contador] = $i;
        $contador++;
    }

    /* echo '<pre>';
    print_r($_SESSION['administration_id']);
    echo '</pre>'; */

    /* foreach ($section as $key => $value) {
        echo '<pre>';
        print_r($key);
        echo '-';
        print_r($value);
        echo '<br>';
        echo '</pre>';
    } */


?>

    <!-- FUNCIÓN ACTIVE A.SOLUCIONES MENÚ -->
    <script type="text/javascript">
        document.getElementById('li-transparencia').className += " active";
    </script>

    <section class="content">
        <div class="container-fluid">

            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">Tabla Transparencia</h4>
                            </li>
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="index.php?view=home">
                                    <i class="fas fa-home"></i>Inicio</a>
                            </li>

                            <li class="breadcrumb-item bcrumb-2">
                                <a href="#" onClick="return false;">Administración</a>
                            </li>

                            <li class="breadcrumb-item active">Transparencia</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">

                        <div class="header d-flex align-items-center justify-content-between">
                            <h2>
                                Tabla <strong>Transparencia</strong>
                            </h2>

                            <button type="button" class="btn btn-success btn-border-radius waves-effect" data-toggle="modal" data-target="#add_fraction"><i class="ti-plus" style="font-size: 10px; margin-right: 5px;"></i>Agregar</button>

                        </div>

                        <div class="body">
                            <!-- <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    <div class="support-box text-center l-bg-red">
                                        <div class="icon m-b-10">
                                            <div class="chart chart-bar">6,4,8,6,8,10,5,6,7,9,5,6,4,8,6,8,10,5,6,7,9,5</div>
                                        </div>
                                        <div class="text m-b-10">Total de fracciones</div>
                                        <h3 class="m-b-0">1512
                                            <i class="material-icons">trending_up</i>
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="support-box text-center l-bg-cyan">
                                        <div class="icon m-b-10">
                                            <span class="chart chart-line">9,4,6,5,6,4,7,3</span>
                                        </div>
                                        <div class="text m-b-10">Noticias no </div>
                                        <h3 class="m-b-0">1236
                                            <i class="material-icons">trending_up</i>
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="support-box text-center l-bg-orange">
                                        <div class="icon m-b-10">
                                            <div class="chart chart-pie">30, 35, 25, 8</div>
                                        </div>
                                        <div class="text m-b-10">Resolve</div>
                                        <h3 class="m-b-0">1107
                                            <i class="material-icons">trending_down</i>
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="support-box text-center green">
                                        <div class="icon m-b-10">
                                            <div class="chart chart-bar">4,6,-3,-1,2,-2,4,3,6,7,-2,3,4,6,-3,-1,2,-2,4,3,6,7,-2,3</div>
                                        </div>
                                        <div class="text m-b-10">Pending</div>
                                        <h3 class="m-b-0">167
                                            <i class="material-icons">trending_down</i>
                                        </h3>
                                    </div>
                                </div>
                            </div> -->

                            <div class="table-responsive">
                                <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                    <thead class="text-center">
                                        <tr>
                                            <th class="center">#</th>
                                            <th class="center">Img</th>
                                            <th class="center">Fracción</th>
                                            <th class="center">Descripción</th>
                                            <th class="center">Departamento responsable</th>
                                            <th class="center">Tipo de fracción</th>
                                            <th class="center">Fecha de validación</th>
                                            <th class="center">Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $count = 0;
                                        foreach ($fraction['fractions'] as $n) :
                                            $count++ ?>
                                            <tr>

                                                <td><?php echo $count ?></td>

                                                <?php if ($n['filename'] == '' || $n['filename'] == NULL) : ?>
                                                    <td class="table-img center">
                                                        <img src="assets/images/user/user.png" width="40" height="40" alt="<?php echo $n['filename']; ?>">
                                                    </td>
                                                <?php else : ?>
                                                    <td class="table-img center">
                                                        <img src="files/iconFraction/<?php echo $n['filename']; ?>" width="40" height="40" alt="<?php echo $n['filename']; ?>">
                                                    </td>
                                                <?php endif; ?>

                                                <td class="center"><?php echo $n['fraction']; ?></td>

                                                <td class="center"><?php echo $n['description']; ?></td>

                                                <td>
                                                    <?php if ($n['department_id'] == 1) : ?>
                                                        <button class="btn-hover btn-border-radius color-8"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 2) : ?>
                                                        <button class="btn-hover btn-border-radius color-3"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 3) : ?>
                                                        <button class="btn-hover btn-border-radius color-8"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 4) : ?>
                                                        <button class="btn-hover btn-border-radius color-7"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 5) : ?>
                                                        <button class="btn-hover btn-border-radius color-4"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 6) : ?>
                                                        <button class="btn-hover btn-border-radius color-7"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 7) : ?>
                                                        <button class="btn-hover btn-border-radius color-8"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 8) : ?>
                                                        <button class="btn-hover btn-border-radius color-5"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 9) : ?>
                                                        <button class="btn-hover btn-border-radius color-6"><?php echo $n['departments'] ?></button>
                                                    <?php else : ?>
                                                        <button class="btn-hover btn-border-radius color-7"><?php echo $n['departments'] ?></button>
                                                    <?php endif; ?>
                                                </td>

                                                <td class="center"><?php echo $n['type']; ?></td>

                                                <td class="center"><?php echo explode(" ", $n['date_validation'])[0]; ?></td>

                                                <td style="padding: 10px 0;">
                                                    <button class="btn tblActnBtn" id="fraction" onclick="fraction_id(this)" data-fraction_id="<?php echo $n['id'] ?>" data-toggle="modal" data-target="#edit_fraction">
                                                        <i class="icon-note blue-text"></i>
                                                    </button>

                                                    <button class="btn tblActnBtn">
                                                        <a href="index.php?action=delfraction&id=<?php echo $n['id']; ?>" class="delete">
                                                            <i class="icon-trash red-text"></i>
                                                        </a>
                                                    </button>

                                                    <button class="btn tblActnBtn" id="btn_seccion" onclick="section_id(this)" data-fraction_id="<?php echo $n['id'] ?>" data-toggle="modal" data-target="#mod_section">
                                                        <i class="icon-list orange-text"></i>
                                                    </button>

                                                    <!-- <?php if ($n['publish'] != 1) : ?>
                                                        <a class="publish" href="index.php?action=publishnew&id=<?php echo $n['id']; ?>&type=<?php echo  $n['type'] ?>">
                                                            <button class="btn tblActnBtn">
                                                                <i class="icon-share-alt green-text"></i>
                                                            </button>
                                                        </a>
                                                    <?php else : ?>
                                                        <a class="unpublish" href="index.php?action=unpublishnew&id=<?php echo $n['id']; ?>">
                                                            <button class="btn tblActnBtn">
                                                                <i class="icon-ban orange-text"></i>
                                                            </button>
                                                        </a>
                                                    <?php endif; ?> -->
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>

                                    <tfoot>
                                        <tr>
                                            <th class="center">#</th>
                                            <th class="center">Img</th>
                                            <th class="center">Fracción</th>
                                            <th class="center">Descripción</th>
                                            <th class="center">Departamento responsable</th>
                                            <th class="center">Tipo de fracción</th>
                                            <th class="center">Fecha de validación</th>
                                            <th class="center">Acciones</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal Para agragar fracccion  -->
    <div class="modal fade" id="add_fraction" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Agregar una nueva fracción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="form_validation" action="index.php?action=addfraction" enctype="multipart/form-data" method="POST">

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Icono de la fracción* (192 x 192)</span>
                                <input type="file" name="filename" id="filename">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input id="fraction" type="text" class="form-control" name="fraction" required>
                                <label class="form-label">Fracción* (en numero romano)</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input id="fraction_name" type="text" class="form-control" name="name" required>
                                <label class="form-label">Nombre*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <label>Descripción</label>
                                <textarea name="description" id="description_fraction" rows="10" cols="80"></textarea>
                            </div>
                        </div>

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <input id="update_date_add" name="update_date" type="text" class="datepicker2 form-control" placeholder="Seleccione una fecha de Actualización">
                                </div>
                            </div>

                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <input id="date_validity_add" name="date_validation" type="text" class="datepicker2 form-control" placeholder="Seleccione una fecha de Validacion">
                                </div>
                            </div>
                        </div>

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <!-- <label>Departamento*</label> -->
                                    <div class="input-field ">
                                        <select id="type_fraction" name="type_id">
                                            <option value="" disabled selected>Elige un Tipo de fracción*</option>
                                            <?php foreach ($typefraction['TypeFractions'] as $t) : ?>
                                                <option value="<?php echo $t['id']; ?>"><?php echo $t['type'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <!-- <label>Departamento*</label> -->
                                    <div class="input-field ">
                                        <select id="departments_fraction" name="department_id" multiple="multiple">
                                            <option value="" disabled selected>Elige un Departamento*</option>
                                            <?php foreach ($departments['departments'] as $d) : ?>
                                                <option value="<?php echo $d['id']; ?>"><?php echo $d['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <div class="input-field ">
                                        <select id="administration_fraction" name="administration_id">
                                            <option value="" disabled selected>Elige una administración*</option>
                                            <?php foreach ($administration['administration'] as $a) : ?>
                                                <option value="<?php echo $a['id']; ?>"><?php echo $a['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <div class="input-field ">
                                        <select id="update_period_fraction" name="update_period">
                                            <option value="" disabled selected>Elige un Periodo*</option>
                                            <?php foreach ($period['Period'] as $p) : ?>
                                                <option value="<?php echo $p['id']; ?>"><?php echo $p['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input id="format_fraction_add" type="text" class="form-control" name="format" required>
                                <label class="form-label">Formato de la Fracción</label>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Agregar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Para Editar fracccion  -->
    <div class="modal fade" id="edit_fraction" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Editar Fracción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="updatefraction" action="index.php?action=updatefraction" enctype="multipart/form-data" method="POST">

                        <input type="text" id="id_fraction" name="id" class="form-control" value="" hidden>

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Icono de la fracción* (192 x 192)</span>
                                <input type="file" name="filename">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input id="fraction_edit" type="text" class="form-control" name="fraction" required>
                                <label class="form-label">Fracción* (en numero romano)</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input id="name_user_edit" type="text" class="form-control" name="name" required>
                                <label class="form-label">Nombre*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <label>Descripción</label>
                                <textarea name="description" id="description_edit" rows="10" cols="80"></textarea>
                            </div>
                        </div>

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <input id="update_date_edit" name="update_date" type="text" class="datepicker2 form-control" placeholder="Seleccione una fecha de Actualización">
                                </div>
                            </div>

                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <input id="date_validity_edit" name="date_validation" type="text" class="datepicker2 form-control" placeholder="Seleccione una fecha de Validacion">
                                </div>
                            </div>
                        </div>

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <!-- <label>Departamento*</label> -->
                                    <div class="input-field ">
                                        <select id="type_edit" name="type_id">
                                            <option value="" disabled selected>Elige un Tipo de fracción*</option>
                                            <?php foreach ($typefraction['TypeFractions'] as $t) : ?>
                                                <option value="<?php echo $t['id']; ?>"><?php echo $t['type'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <div class="input-field ">
                                        <select id="department_edit" name="department_id" multiple="multiple">
                                            <option value="" disabled selected>Elige un Departamento*</option>
                                            <?php foreach ($departments['departments'] as $c) : ?>
                                                <option value="<?php echo $c['id']; ?>"><?php echo $c['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <div class="input-field ">
                                        <select id="update_period_edit" name="update_period">
                                            <option value="" disabled selected>Elige un Periodo*</option>
                                            <?php foreach ($period['Period'] as $p) : ?>
                                                <option value="<?php echo $p['id']; ?>"><?php echo $p['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input id="format_fraction_edit" type="text" class="form-control" name="format" required>
                                <label class="form-label">Formato de la Fracción</label>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Editar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal secciones  -->
    <div class="modal fade" id="mod_section" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true" style="background-color: rgb(238 238 238 / 0%);">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 1050px;">
            <div class="modal-content dark">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Secciones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="card">

                                <div class="header d-flex align-items-center justify-content-between">
                                    <h2>
                                        <strong>Secciones</strong> de la Fracción
                                    </h2>

                                    <button type="button" class="btn btn-success btn-border-radius waves-effect" data-toggle="modal" data-target="#add_section"><i class="ti-plus" style="font-size: 10px; margin-right: 5px;"></i>Agregar</button>

                                </div>

                                <div class="body">

                                    <div class="table-responsive" style="overflow-x: clip;">
                                        <table id="" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                            <thead class="text-center">
                                                <tr>
                                                    <th class="center">Seccion</th>
                                                    <th class="center">Descripción</th>
                                                    <th class="center">Acciones</th>
                                                </tr>
                                            </thead>

                                            <tbody id="tab_section">

                                            </tbody>

                                            <tfoot>
                                                <tr>
                                                    <th class="center">Seccion</th>
                                                    <th class="center">Descripción</th>
                                                    <th class="center">Acciones</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!-- Modal Para agregar seccion  -->
    <div class="modal fade" id="add_section" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Agregar una nueva sección de la fracción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="FormAddSeccion" action="./index.php?action=addsection" enctype="multipart/form-data" method="POST">

                        <input type="text" hidden id="id_frac_sec" name="fraction_id" class="form-control">

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <!-- <label>Departamento*</label> -->
                                    <div class="input-field ">
                                        <select id="sel_add_section" name="name">
                                            <option value="" disabled selected>Elige una seccion (año)*</option>
                                            <?php foreach ($section as $key => $value) : ?>
                                                <option value="<?php echo $value; ?>"><?php echo $value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <label>Descripción</label>
                                <textarea name="description" id="description_editt" rows="10" cols="80"></textarea>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Agregar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Para editar seccion  -->
    <div class="modal fade" id="edit_section" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Editar seccion de la fracción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="updatesection" action="index.php?action=updatesection" enctype="multipart/form-data" method="POST">

                        <input type="text" hidden id="section_id" name="id" class="form-control" value="">

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <!-- <label>Departamento*</label> -->
                                    <div class="input-field ">
                                        <select id="sel_edit_section" name="name">
                                            <option value="" disabled selected>Elige una seccion (año)*</option>
                                            <?php foreach ($section as $key => $value) : ?>
                                                <option value="<?php echo $value; ?>"><?php echo $value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <label>Descripción</label>
                                <textarea name="description" id="des_edit_section" rows="10" cols="80"></textarea>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Editar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal periodo  -->
    <div class="modal fade" id="periodo" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true" style="background-color: rgb(238 238 238 / 0%);">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 1050px;">
            <div class="modal-content dark">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Periodos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="card">

                                <div class="header d-flex align-items-center justify-content-between">
                                    <h2>
                                        <strong>Periodos</strong> de la sección
                                    </h2>

                                    <button type="button" class="btn btn-success btn-border-radius waves-effect" data-toggle="modal" data-target="#add_periodo"><i class="ti-plus" style="font-size: 10px; margin-right: 5px;"></i>Agregar</button>

                                </div>

                                <div class="body">

                                    <div class="table-responsive" style="overflow-x: clip;">
                                        <table id="" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                            <thead class="text-center">
                                                <tr>
                                                    <th class="center">Periodo</th>
                                                    <th class="center">Descripción</th>
                                                    <th class="center">Acciones</th>
                                                </tr>
                                            </thead>

                                            <tbody id="tab_period">

                                            </tbody>

                                            <tfoot>
                                                <tr>
                                                    <th class="center">Periodo</th>
                                                    <th class="center">Descripción</th>
                                                    <th class="center">Acciones</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!-- Modal Para agragar Periodo  -->
    <div class="modal fade" id="add_periodo" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Agregar un nuevo periodo de la sección</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="AddPeriodo" action="index.php?action=addperiodo" enctype="multipart/form-data" method="POST">

                        <input type="text" hidden id="id_section_add" name="subsection" class="form-control">

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input id="period_name_add" type="text" class="form-control" name="name" required>
                                <label class="form-label">Nombre*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <label>Descripción</label>
                                <textarea name="description" id="des_add_period" rows="10" cols="80"></textarea>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Agregar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Para Editar Periodo  -->
    <div class="modal fade" id="edit_periodo" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Editar periodo de la sección</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="updateperiod" action="index.php?action=updateperiodo" enctype="multipart/form-data" method="POST">

                        <input type="text" hidden id="id_section_edit" name="id" class="form-control">

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input id="period_name_edit" type="text" class="form-control" name="name" required>
                                <label class="form-label">Nombre*</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <label>Descripción</label>
                                <textarea name="description" id="des_edit_period" rows="10" cols="80"></textarea>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Editar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal elemento  -->
    <div class="modal fade" id="elemento" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true" style="background-color: rgb(238 238 238 / 0%);">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 1050px;">
            <div class="modal-content dark">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Elementos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="card">

                                <div class="header d-flex align-items-center justify-content-between">
                                    <h2>
                                        <strong>Elementos</strong> de la sección
                                    </h2>

                                    <button type="button" class="btn btn-success btn-border-radius waves-effect" data-toggle="modal" data-target="#add_element"><i class="ti-plus" style="font-size: 10px; margin-right: 5px;"></i>Agregar</button>

                                </div>

                                <div class="body">

                                    <div class="table-responsive" style="overflow-x: clip;">
                                        <table id="" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                            <thead class="text-center">
                                                <tr>
                                                    <th class="center">Enlace</th>
                                                    <th class="center">Descripción</th>
                                                    <th class="center">Acciones</th>
                                                </tr>
                                            </thead>

                                            <tbody id="tab_element">

                                            </tbody>

                                            <tfoot>
                                                <tr>
                                                    <th class="center">Enlace</th>
                                                    <th class="center">Descripción</th>
                                                    <th class="center">Acciones</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!-- Modal Para agregar elemento  -->
    <div class="modal fade" id="add_element" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Agregar un nuevo elemento de la sección</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="FormAddElemento" action="index.php?action=addelement" enctype="multipart/form-data" method="POST">

                        <input type="text" hidden id="id_element_add" name="section_id" class="form-control">

                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" id="link_add_element" class="form-control" name="link">
                                <label class="form-label">Link</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" id="des_add_element" class="form-control" name="description">
                                <label class="form-label">Descripción</label>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Agregar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Para Editar elemento  -->
    <div class="modal fade" id="edit_element" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Editar elemento de la sección</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="updateelement" action="index.php?action=updateelement" enctype="multipart/form-data" method="POST">

                        <input type="text" hidden id="id_element_edit" name="id" class="form-control">

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input type="text" id="link_edit_element" class="form-control" name="link">
                                <label class="form-label">Link</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input type="text" id="des_edit_element" class="form-control" name="description">
                                <label class="form-label">Descripción</label>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Editar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <script>
        var description_fraction;
        ClassicEditor.create(document.querySelector("#description_fraction"))
            .then((editor) => {
                description_fraction = editor;
            })
            .catch((error) => {
                console.error(error);
            });
    </script>
    <!-- Custom Js -->
    <script src="assets/js/edits_ajax/edit_fraction.js"></script>
    <script src="assets/js/edits_ajax/edit_section.js"></script>
    <script src="assets/js/edits_ajax/edit_period.js"></script>
    <script src="assets/js/edits_ajax/edit_element.js"></script>
    <script src="assets/js/pages/ui/dialogs.js"></script>
    <!-- Export table -->
    <script src="assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="assets/js/pages/apps/support.js"></script>

<?php else : Core::redir("index.php?view=home");
endif; ?>