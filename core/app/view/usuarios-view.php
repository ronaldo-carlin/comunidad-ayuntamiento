<?php
$permissions = UserData::getPermissonsByUserAndModule($_SESSION["user_id"], 17);

if ($permissions->read == 1) :

    $u = UserData::getUserByIdAPI($_SESSION["user_id"]);
    $users = UserData::getUsersActivesAPI(1);
    $departments = DepartmentsData::getDepartments();
    $rol = RolesData::getActivesAPI();
    $area = AreasData::getActivesAPI();
?>

    <section class="content">
        <div class="container-fluid">

            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">Tabla Usuarios</h4>
                            </li>
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="index.php?view=home">
                                    <i class="fas fa-home"></i>Inicio</a>
                            </li>

                            <li class="breadcrumb-item bcrumb-2">
                                <a href="#" onClick="return false;">Administración</a>
                            </li>

                            <li class="breadcrumb-item active">Usuarios</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">

                        <div class="header d-flex align-items-center justify-content-between">
                            <h2>
                                Tabla <strong>Usuarios</strong>
                            </h2>

                            <button type="button" class="btn btn-success btn-border-radius waves-effect" data-toggle="modal" data-target="#addUser"><i class="ti-plus" style="font-size: 10px; margin-right: 5px;"></i>Agregar</button>

                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <!-- <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100"> -->
                                <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">

                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Telefono</th>
                                            <th>Cumpleaños</th>
                                            <th>Departamento</th>
                                            <th>Status</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $count = 0;
                                        foreach ($users['users'] as $u) :
                                            $count++ ?>
                                            <tr>
                                                <td>
                                                    <?php echo $count ?>
                                                </td>

                                                <td><?php echo $u['name']; ?></td>
                                                <td><?php echo $u['email']; ?></td>
                                                <td><?php echo $u['phone']; ?></td>
                                                <td><?php echo $u['date_birth']; ?></td>
                                                <td>
                                                    <?php if ($u['department_id'] == 1) : ?>
                                                        <button class="btn-hover btn-border-radius color-8"><?php echo $u['departments'] ?></button>
                                                    <?php elseif ($u['department_id'] == 2) : ?>
                                                        <button class="btn-hover btn-border-radius color-3"><?php echo $u['departments'] ?></button>
                                                    <?php elseif ($u['department_id'] == 3) : ?>
                                                        <button class="btn-hover btn-border-radius color-8"><?php echo $u['departments'] ?></button>
                                                    <?php elseif ($u['department_id'] == 4) : ?>
                                                        <button class="btn-hover btn-border-radius color-7"><?php echo $u['departments'] ?></button>
                                                    <?php elseif ($u['department_id'] == 5) : ?>
                                                        <button class="btn-hover btn-border-radius color-4"><?php echo $u['departments'] ?></button>
                                                    <?php elseif ($u['department_id'] == 6) : ?>
                                                        <button class="btn-hover btn-border-radius color-7"><?php echo $u['departments'] ?></button>
                                                    <?php elseif ($u['department_id'] == 7) : ?>
                                                        <button class="btn-hover btn-border-radius color-8"><?php echo $u['departments'] ?></button>
                                                    <?php elseif ($u['department_id'] == 8) : ?>
                                                        <button class="btn-hover btn-border-radius color-5"><?php echo $u['departments'] ?></button>
                                                    <?php elseif ($u['department_id'] == 9) : ?>
                                                        <button class="btn-hover btn-border-radius color-6"><?php echo $u['departments'] ?></button>
                                                    <?php else : ?>
                                                        <button class="btn-hover btn-border-radius color-7"><?php echo $u['departments'] ?></button>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($u['is_active'] == 1) : ?>
                                                        <span class="pull-right progress-percent label label-success m-b-5">Activo</span>
                                                    <?php else : ?>
                                                        <span class="pull-right progress-percent label label-danger m-b-5">Inactivo</span>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <button class="btn tblActnBtn " id="user_id" onclick="user_id(this)" data-user_id="<?php echo $u['id'] ?>" data-toggle="modal" data-target="#editUser">
                                                        <i class="icon-note blue-text"></i>
                                                    </button>
                                                    <button class="btn tblActnBtn">
                                                        <a href="index.php?action=deluser&id=<?php echo $u['id']; ?>" class="delete">

                                                            <i class="icon-trash red-text "></i>

                                                        </a>
                                                    </button>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal Para Agregar -->
    <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Agregar un nuevo usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <div class="modal-body">
                    <form id="form_validation" action="index.php?action=adduser" enctype="multipart/form-data" method="POST">

                        <small>--- Información de la cuenta ---</small>

                        <div class="my-3">

                            <!-- nombre y corre -->
                            <div class="row clearfix">

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" required>
                                            <label class="form-label">Nombre Completo*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="email" required>
                                            <label class="form-label">Correo eletronico / Usuario*</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- contraseña y foto de perfil -->
                            <div class="row clearfix align-items-end">
                                <div class="col-md-6 col-sm-6 d-flex align-items-end">
                                    <div class="col-10 p-0">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <label class="form-label">Contraseña*</label>
                                                <input type="password" class="form-control" name="password" id="password" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1 p-0">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <button class="form-control p-0" type="button" onclick="mostrarPasswordadd()">
                                                    <span class="fa fa-eye-slash icon" style="font-size: 20px;"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- imagen de perfil -->
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <label class="">Imagen de perfil*</label>
                                        <div class="form-line">
                                            <input type="file" class="default" name="img" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <small>--- información del perfil ---</small>

                        <div class="my-3">

                            <!-- fecha de nacimiento y telefono -->
                            <div class="row clearfix align-items-end">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="date_birth" type="text" class="datepicker form-control" placeholder="Fecha de nacimiento*">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="phone" required>
                                            <label class="form-label">Telefono*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- direccion del usuario -->
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="address">
                                            <label class="form-label">Direccion</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- crp, rfc, nss -->
                            <div class="row clearfix">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="curp">
                                            <label class="form-label">Curp</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="rfc">
                                            <label class="form-label">RFC</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="imss">
                                            <label class="form-label">NSS</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <small>--- información de puesto ---</small>

                        <div class="my-3">
                            <!-- fecha de ingreso y departamemtos -->
                            <div class="row clearfix align-items-end">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input name="date_admission" id="fech_add" type="text" class="datepicker form-control" placeholder="Fecha de ingreso">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <!-- <label>Departamento*</label> -->
                                            <div class="input-field ">
                                                <select name="department_id">
                                                    <option value="" disabled selected>Elige una departamento*</option>
                                                    <?php foreach ($departments['departments'] as $c) : ?>
                                                        <option value="<?php echo $c['id']; ?>"><?php echo $c['name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- areas y rol -->
                            <div class="row clearfix align-items-end">

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <!-- <label>Area*</label> -->
                                            <div class="input-field ">
                                                <select name="area_id">
                                                    <option value="" disabled selected>Elige una area*</option>
                                                    <?php foreach ($area['areas'] as $a) : ?>
                                                        <option value="<?php echo $a['id']; ?>"><?php echo $a['name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <!-- <label>Rol*</label> -->
                                            <div class="input-field ">
                                                <select name="role">
                                                    <option value="" disabled selected>Elige un rol*</option>
                                                    <?php foreach ($rol['roles'] as $r) : ?>
                                                        <option value="<?php echo $r['id']; ?>"><?php echo $r['name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- botn de enviar -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Agregar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Para editar -->
    <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Editar usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <div class="modal-body">
                    <form id="form_validation" action="index.php?action=updateuser" enctype="multipart/form-data" method="POST">
                        
                        <small>--- Información de la cuenta ---</small>

                        <input type="text" hidden id="id" name="id" class="form-control" value="">
                        <input type="text" hidden id="role_id" name="role_id" class="form-control" value="1">
                        
                        <div class="my-3">

                            <!-- nombre y correo -->
                            <div class="row clearfix">

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="name_user_edit" name="name" required>
                                            <label class="form-label">Nombre Completo*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="email_user" name="email" required>
                                            <label class="form-label">Correo eletronico / Usuario*</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- contraseña y foto de perfil -->
                            <div class="row clearfix align-items-end">

                                <!-- imagen de perfil -->
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <label class="">Imagen de perfil*</label>
                                        <div class="form-line">
                                            <input type="file" class="default" name="img">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <small>--- información del perfil ---</small>

                        <div class="my-3">

                            <!-- fecha de nacimiento y telefono -->
                            <div class="row clearfix align-items-end">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input id="date_birth_user" name="date_birth" type="text" class="datepicker form-control" placeholder="Fecha de nacimiento*" require>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="phone_user" name="phone" required>
                                            <label class="form-label">Telefono*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- direccion del usuario -->
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="address_user" name="address">
                                            <label class="form-label">Direccion</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- crp, rfc, nss -->
                            <div class="row clearfix">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="curp_user" name="curp">
                                            <label class="form-label">Curp</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="rfc_user" name="rfc">
                                            <label class="form-label">RFC</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="imss_user" name="imss">
                                            <label class="form-label">NSS</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <small>--- información de puesto ---</small>

                        <div class="my-3">
                            <!-- fecha de ingreso y departamemtos -->
                            <div class="row clearfix align-items-end">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input id="date_admission_user" name="date_admission" type="text" class="datepicker form-control" placeholder="Fecha de ingreso">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <!-- <label>Departamento*</label> -->
                                            <div class="input-field ">
                                                <select name="department_id" id="department_user">
                                                    <option value="" disabled selected>Elige una departamento*</option>
                                                    <?php foreach ($departments['departments'] as $c) : ?>
                                                        <option value="<?php echo $c['id']; ?>"><?php echo $c['name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- areas y rol -->
                            <div class="row clearfix align-items-end">

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <!-- <label>Area*</label> -->
                                            <div class="input-field ">
                                                <select name="area_id" id="area_user">
                                                    <option value="" disabled selected>Elige una area*</option>
                                                    <?php foreach ($area['areas'] as $a) : ?>
                                                        <option value="<?php echo $a['id']; ?>"><?php echo $a['name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <!-- <label>Rol*</label> -->
                                            <div class="input-field ">
                                                <select id="role_id_user" name="role">
                                                    <option value="" disabled selected>Elige un rol*</option>
                                                    <?php foreach ($rol['roles'] as $r) : ?>
                                                        <option value="<?php echo $r['id']; ?>"><?php echo $r['name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- botn de enviar -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Agregar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <script src="assets/js/edits_ajax/edit_user.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/pages/ui/dialogs.js"></script>
    <!-- Export table -->
    <script src="assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="assets/js/pages/apps/support.js"></script>

    <script type="text/javascript">
        function mostrarPasswordadd() {
            var cambio = document.getElementById("password");
            if (cambio.type == "password") {
                cambio.type = "text";
                $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
            } else {
                cambio.type = "password";
                $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
            }
        }

        function mostrarPasswordedit() {
            var cambio = document.getElementById("password_user");
            if (cambio.type == "password") {
                cambio.type = "text";
                $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
            } else {
                cambio.type = "password";
                $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
            }
        }
    </script>

<?php else : Core::redir("index.php?view=home");
endif ?>