<?php
$permissions = UserData::getPermissonsByUserAndModule($_SESSION["user_id"], 6);

if ($permissions->read == 1) :

    $u = UserData::getUserByIdAPI($_SESSION["user_id"]);
    $departments = DepartmentsData::getDepartments();
?>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style">
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="index.php?view=home"><i class="fas fa-home"></i>Inicio</a>
                            </li>
                            <li class="breadcrumb-item active">Calendario</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>Draggable</strong> Events
                            </h2>
                        </div>
                        <div class="body">
                            <div id='external-events'>
                                <form class="inline-form">
                                    <input type="text" value="" class="form-control" placeholder="Event Title..." id="event_title" />
                                    <button type="button" id="event_add" class="btn btn-primary waves-effect m-t-15">Add
                                        Event
                                    </button>
                                </form>
                                <div id="event_box" class="mg-bottom-10"></div>
                                <label>
                                    <input type="checkbox" id='drop-remove' />
                                    <span>remove after drop</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>Events</strong>
                            </h2>
                        </div>
                        <div class="body">
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <script src="assets/js/pages/apps/calendar.js"></script>

<?php else : Core::redir("./index.php?view=home");
endif ?>