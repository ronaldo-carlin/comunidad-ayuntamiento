<?php
$permissions = UserData::getPermissonsByUserAndModule($_SESSION["user_id"], 14);

if ($permissions->read == 1) :

    $u = UserData::getById($_SESSION["user_id"]);
    $rol = RolesData::getActivesAPI();

    $news = NewsData::getAllNews();
    $departments = DepartmentsData::getDepartments();

    /* $newsAllCount = NewsData::getAllCount()->count;
$newsPublishCount = NewsData::getPublishCount()->count;
$newsNoPublishCount = NewsData::getNopublishCount()->count;

$percentAll = ($newsAllCount * 100) / $newsAllCount;
$percentPublish = ($newsPublishCount * 100) / $newsAllCount;
$percentNoPublish = ($newsNoPublishCount * 100) / $newsAllCount; */
?>

    <!-- FUNCIÓN ACTIVE A.SOLUCIONES MENÚ -->
    <script type="text/javascript">
        document.getElementById('li-noticias_admin').className += " active";
    </script>

    <section class="content">
        <div class="container-fluid">

            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">Tabla Noticias</h4>
                            </li>
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="index.php?view=home">
                                    <i class="fas fa-home"></i>Inicio</a>
                            </li>

                            <li class="breadcrumb-item bcrumb-2">
                                <a href="#" onClick="return false;">Administración</a>
                            </li>

                            <li class="breadcrumb-item active">Noticias</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">

                        <div class="header d-flex align-items-center justify-content-between">
                            <h2>
                                Tabla <strong>Noticias</strong>
                            </h2>

                            <button type="button" class="btn btn-success btn-border-radius waves-effect" data-toggle="modal" data-target="#add_news"><i class="ti-plus" style="font-size: 10px; margin-right: 5px;"></i>Agregar</button>

                        </div>

                        <div class="body">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6">
                                    <div class="support-box text-center l-bg-red">
                                        <div class="icon m-b-10">
                                            <div class="chart chart-bar">6,4,8,6,8,10,5,6,7,9,5,6,4,8,6,8,10,5,6,7,9,5</div>
                                        </div>
                                        <div class="text m-b-10">Total de Noticias</div>
                                        <h3 class="m-b-0">1512
                                            <i class="material-icons">trending_up</i>
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="support-box text-center l-bg-cyan">
                                        <div class="icon m-b-10">
                                            <span class="chart chart-line">9,4,6,5,6,4,7,3</span>
                                        </div>
                                        <div class="text m-b-10">Noticias no Publicadas</div>
                                        <h3 class="m-b-0">1236
                                            <i class="material-icons">trending_up</i>
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="support-box text-center l-bg-orange">
                                        <div class="icon m-b-10">
                                            <div class="chart chart-pie">30, 35, 25, 8</div>
                                        </div>
                                        <div class="text m-b-10">Resolve</div>
                                        <h3 class="m-b-0">1107
                                            <i class="material-icons">trending_down</i>
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="support-box text-center green">
                                        <div class="icon m-b-10">
                                            <div class="chart chart-bar">4,6,-3,-1,2,-2,4,3,6,7,-2,3,4,6,-3,-1,2,-2,4,3,6,7,-2,3</div>
                                        </div>
                                        <div class="text m-b-10">Pending</div>
                                        <h3 class="m-b-0">167
                                            <i class="material-icons">trending_down</i>
                                        </h3>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                    <!-- <table class="table table-hover js-basic-example contact_list"> -->
                                    <thead class="text-center">
                                        <tr>
                                            <th class="center">#</th>
                                            <th class="center">Img</th>
                                            <th class="center">Titulo</th>
                                            <th class="center">Creado por</th>
                                            <th class="center">Departamento</th>
                                            <th class="center">Publicada</th>
                                            <th class="center">Fecha de Publicación</th>
                                            <th class="center">Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $count = 0;
                                        foreach ($news['news'] as $n) :
                                            $count++ ?>
                                            <tr>

                                                <td><?php echo $count ?></td>

                                                <td class="table-img center">
                                                    <img src="files/imgNews/<?php echo $n['cover_page']; ?>" width="40" height="40" alt="">
                                                </td>

                                                <td class="center"><?php echo $n['title']; ?></td>

                                                <!-- <td class="center"><?php echo  UserData::getById($n['created_by'])->name ?></td> -->
                                                <td class="center"><?php echo  $n['created_by']; ?></td>

                                                <td>
                                                    <?php if ($n['department_id'] == 1) : ?>
                                                        <button class="btn-hover btn-border-radius color-8"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 2) : ?>
                                                        <button class="btn-hover btn-border-radius color-3"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 3) : ?>
                                                        <button class="btn-hover btn-border-radius color-8"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 4) : ?>
                                                        <button class="btn-hover btn-border-radius color-7"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 5) : ?>
                                                        <button class="btn-hover btn-border-radius color-4"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 6) : ?>
                                                        <button class="btn-hover btn-border-radius color-7"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 7) : ?>
                                                        <button class="btn-hover btn-border-radius color-8"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 8) : ?>
                                                        <button class="btn-hover btn-border-radius color-5"><?php echo $n['departments'] ?></button>
                                                    <?php elseif ($n['department_id'] == 9) : ?>
                                                        <button class="btn-hover btn-border-radius color-6"><?php echo $n['departments'] ?></button>
                                                    <?php else : ?>
                                                        <button class="btn-hover btn-border-radius color-7"><?php echo $n['departments'] ?></button>
                                                    <?php endif; ?>
                                                </td>

                                                <?php if ($n['publish'] == 1) : ?>
                                                    <td>
                                                        <i class="icon-check green-text ">
                                                            <div style="display:none;"><?php echo $n['publish'] ?></div>
                                                        </i>
                                                    </td>
                                                <?php else : ?>
                                                    <td>
                                                        <i class="icon-close red-text">
                                                            <div style="display:none;"><?php echo $n['publish'] ?></div>
                                                    </td>
                                                <?php endif; ?>

                                                <td class="center"><?php echo explode(" ", $n['created_at'])[0]; ?></td>

                                                <td style="padding: 10px 0;">
                                                    <button class="btn tblActnBtn" id="news_id" onclick="news_id(this)" data-news_id="<?php echo $n['id'] ?>" data-toggle="modal" data-target="#edit_news">
                                                        <i class="icon-note blue-text"></i>
                                                    </button>
                                                    <button class="btn tblActnBtn">
                                                        <a href="index.php?action=delnew&id=<?php echo $n['id']; ?>" class="delete">
                                                            <i class="icon-trash red-text"></i>
                                                        </a>
                                                    </button>
                                                    <?php if ($n['publish'] != 1) : ?>
                                                        <a class="publish" href="index.php?action=publishnew&id=<?php echo $n['id']; ?>&type=<?php echo  $n['type'] ?>">
                                                            <button class="btn tblActnBtn">
                                                                <i class="icon-share-alt green-text"></i>
                                                            </button>
                                                        </a>
                                                    <?php else : ?>
                                                        <a class="unpublish" href="index.php?action=unpublishnew&id=<?php echo $n['id']; ?>">
                                                            <button class="btn tblActnBtn">
                                                                <i class="icon-ban orange-text"></i>
                                                            </button>
                                                        </a>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>

                                    <tfoot>
                                        <tr>
                                            <th class="center">#</th>
                                            <th class="center">Img</th>
                                            <th class="center">Titulo</th>
                                            <th class="center">Creado por</th>
                                            <th class="center">Departamento</th>
                                            <th class="center">Publicada</th>
                                            <th class="center">Fecha</th>
                                            <th class="center">Acciones</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- modal para agergar -->
    <div class="modal fade" id="add_news" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Agregar Nueva Noticia</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="form_validation" action="index.php?action=addnews" enctype="multipart/form-data" method="POST">

                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control" name="title" required>
                                <label class="form-label">Titulo*</label>
                            </div>
                        </div>
                        
                        <div class="form-group form-float">
                            <div class="form-line ">
                                <label>Descripción*</label>
                                <textarea name="description" id="description_add" rows="10" cols="80"></textarea>
                            </div>
                        </div>

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <!-- <label>Departamento*</label> -->
                                    <div class="input-field ">
                                        <select name="departments">
                                            <option value="" disabled selected>Elige un Departamento*</option>
                                            <?php foreach ($departments['departments'] as $c) : ?>
                                                <option value="<?php echo $c['id']; ?>"><?php echo $c['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <input name="date_validity" type="text" class="datepicker form-control" placeholder="Seleccione una fecha de vigencia">
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control" name="link">
                                <label class="form-label">Link</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line ">
                                <input type="text" class="form-control" name="link_youtube">
                                <label class="form-label">Link Youtube</label>
                            </div>
                        </div>

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Imagen de la portada*</span>
                                <input type="file" name="cover_page">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Imagen de la noticia*</span>
                                <input type="file" multiple="multiple" name="multimedia[]">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Video*</span>
                                <input type="file" name="video">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-check m-l-10">
                                <div class="switch">
                                    <label>
                                        <span>Publicar</span>
                                        <input type="checkbox" name="publish" checked>
                                        <span class="lever switch-col-green"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="hidden" name="type" value="1">
                            <button type="submit" class="btn btn-info waves-effect">Agregar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Para Editar  -->
    <div class="modal fade" id="edit_news" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Editar Noticia</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="updatenew" action="index.php?action=updatednew" enctype="multipart/form-data" method="POST">

                        <input type="text" hidden id="id" name="id" class="form-control" value="<?php echo $n['id'] ?>">
                        <input type="text" hidden id="type" name="type" class="form-control" value="<?php echo $n['type'] ?>">

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input id="title_edit" type="text" class="form-control" name="title" required>
                                <label class="form-label">Titulo*</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <label>Descripción</label>
                                <textarea name="description" id="description_editt" rows="10" cols="80"></textarea>
                            </div>
                        </div>

                        <div class="d-flex align-items-center">
                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <!-- <label>Departamento*</label> -->
                                    <div class="input-field ">
                                        <select id="departments_edit" name="departments">
                                            <option value="" disabled selected>Elige un Departamento*</option>
                                            <?php foreach ($departments['departments'] as $c) : ?>
                                                <option value="<?php echo $c['id']; ?>"><?php echo $c['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group form-float px-2">
                                <div class="form-line ">
                                    <input id="date_validity_edit" name="date_validity" type="text" class="datepicker form-control" placeholder="Seleccione una fecha de vigencia">
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input id="link_edit" type="text" class="form-control" name="link">
                                <label class="form-label">Link</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line focused">
                                <input id="link_youtube_edit" type="text" class="form-control" name="link_youtube">
                                <label class="form-label">Link Youtube</label>
                            </div>
                        </div>

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Imagen de la portada*</span>
                                <input type="file" name="cover_page">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Imagen de la noticia*</span>
                                <input type="file" multiple="multiple" name="multimedia[]">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Video*</span>
                                <input type="file" name="video">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-check m-l-10">
                                <div class="switch">
                                    <label>
                                        <span>Publicar</span>
                                        <input id="publish_edit" type="checkbox" name="publish" checked>
                                        <span class="lever switch-col-green"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Editar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <script>
        var description_add;
        ClassicEditor.create(document.querySelector("#description_add"))
            .then((editor) => {
                description_add = editor;
            })
            .catch((error) => {
                console.error(error);
            });

        /* var description_edit;
        ClassicEditor.create(document.querySelector("#description_edit"))
            .then((editor) => {
                description_edit = editor;
            })
            .catch((error) => {
                console.error(error);
            }); */
    </script>

    <!-- Plugins Js -->

    <script src="assets/js/edits_ajax/edit_new.js"></script>
    <script src="assets/js/pages/ui/dialogs.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/pages/ui/dialogs.js"></script>
    <!-- Export table -->
    <script src="assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="assets/js/pages/apps/support.js"></script>

<?php else : Core::redir("index.php?view=home");
endif ?>