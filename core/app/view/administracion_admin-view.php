<?php
$permissions = UserData::getPermissonsByUserAndModule($_SESSION["user_id"], 1);

if ($permissions->read == 1) :

    $u = UserData::getUserByIdAPI($_SESSION["user_id"]);
    $administracion = AdministrationData::getAllAdministration();
?>

    <section class="content">
        <div class="container-fluid">

            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">Tabla Administraciónes</h4>
                            </li>
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="index.php?view=home">
                                    <i class="fas fa-home"></i>Inicio</a>
                            </li>

                            <li class="breadcrumb-item bcrumb-2">
                                <a href="#" onClick="return false;">Administración</a>
                            </li>

                            <li class="breadcrumb-item active">Administraciónes</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">

                        <div class="header d-flex align-items-center justify-content-between">
                            <h2>
                                Tabla <strong>Administración</strong>
                            </h2>

                            <button type="button" class="btn btn-success btn-border-radius waves-effect" data-toggle="modal" data-target="#addDepartment"><i class="ti-plus" style="font-size: 10px; margin-right: 5px;"></i>Agregar</button>

                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">

                                    <thead class="text-center">
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre del alcande</th>
                                            <th>Fecha de inicio</th>
                                            <th>Fecha de termino</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $count = 0;
                                        foreach ($administracion['administration'] as $d) :
                                            $count++ ?>
                                            <tr>
                                                <td><?php echo $count ?></td>

                                                <td><?php echo $d['name']; ?></td>

                                                <td><?php echo $d['date_start']; ?></td>

                                                <td><?php echo $d['date_end']; ?></td>

                                                <td>
                                                    <button class="btn tblActnBtn " id="department_id" onclick="department_id(this)" data-user_id="<?php echo $d['id'] ?>" data-toggle="modal" data-target="#editDepartment">
                                                        <i class="icon-note blue-text"></i>
                                                    </button>
                                                    <button class="btn tblActnBtn">
                                                        <a href="index.php?action=deluser&id=<?php echo $d['id']; ?>" class="delete">
                                                            <i class="icon-trash red-text "></i>
                                                        </a>
                                                    </button>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>

                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal Para Agregar -->
    <div class="modal fade" id="addDepartment" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Agregar un nuevo departamento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <div class="modal-body">
                    <form id="form_validation" action="index.php?action=adduser" enctype="multipart/form-data" method="POST">

                        <small>--- Información del departamento ---</small>

                        <div class="my-3">

                            <!-- departamento y director -->
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" required>
                                            <label class="form-label">Nombre del Departamento*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="email" required>
                                            <label class="form-label">Nombre del Director*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- botn de enviar -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Agregar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Para editar -->
    <div class="modal fade" id="editDepartment" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="formModal">Editar departamento</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <div class="modal-body">
                    <form id="form_validation" action="index.php?action=updateuser" enctype="multipart/form-data" method="POST">

                        <small>--- Información de la cuenta ---</small>

                        <input type="text" hidden id="id" name="id" class="form-control" value="">
                        <input type="text" hidden id="role_id" name="role_id" class="form-control" value="1">

                        <div class="my-3">

                            <!-- nombre y correo -->
                            <div class="row clearfix">

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="name_user" name="name" required>
                                            <label class="form-label">Nombre Completo*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="email_user" name="email" required>
                                            <label class="form-label">Correo eletronico / Usuario*</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- contraseña y foto de perfil -->
                            <div class="row clearfix align-items-end">
                                <div class="col-md-6 col-sm-6 d-flex align-items-end">

                                    <div class="col-10 p-0">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <label class="form-label">Contraseña*</label>
                                                <input type="password" class="form-control" name="password" id="password_user" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-1 p-0">
                                        <div class="form-group form-float">
                                            <div class="form-line focused">
                                                <button class="form-control p-0" type="button" onclick="mostrarPasswordedit()">
                                                    <span class="fa fa-eye-slash icon" style="font-size: 20px;"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- imagen de perfil -->
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <label class="">Imagen de perfil*</label>
                                        <div class="form-line">
                                            <input type="file" class="default" name="img">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <small>--- información del perfil ---</small>

                        <div class="my-3">

                            <!-- fecha de nacimiento y telefono -->
                            <div class="row clearfix align-items-end">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input id="date_birth_user" name="date_birth" type="text" class="datepicker form-control" placeholder="Fecha de nacimiento">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="phone_user" name="phone" required>
                                            <label class="form-label">Telefono*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- direccion del usuario -->
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="address_user" name="address" required>
                                            <label class="form-label">Direccion*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- crp, rfc, nss -->
                            <div class="row clearfix">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="curp_user" name="curp" required>
                                            <label class="form-label">Curp*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="rfc_user" name="rfc" required>
                                            <label class="form-label">RFC*</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group form-float">
                                        <div class="form-line focused">
                                            <input type="text" class="form-control" id="imss_user" name="imss" required>
                                            <label class="form-label">NSS*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <small>--- información de puesto ---</small>

                        <div class="my-3">
                            <!-- fecha de ingreso y departamemtos -->
                            <div class="row clearfix align-items-end">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input id="date_admission_user" name="date_admission" type="text" class="datepicker form-control" placeholder="Fecha de ingreso ">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <!-- <label>Departamento*</label> -->
                                            <div class="input-field ">
                                                <select name="department_id" id="department_user">
                                                    <option value="" disabled selected>Elige una empresa*</option>
                                                    <?php foreach ($departments['departments'] as $c) : ?>
                                                        <option value="<?php echo $c['id']; ?>"><?php echo $c['name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- areas y rol -->
                            <div class="row clearfix align-items-end">

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <!-- <label>Area*</label> -->
                                            <div class="input-field ">
                                                <select name="area_id" id="area_user">
                                                    <option value="" disabled selected>Elige una area*</option>
                                                    <?php foreach ($area['areas'] as $a) : ?>
                                                        <option value="<?php echo $a['id']; ?>"><?php echo $a['name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <!-- <label>Rol*</label> -->
                                            <div class="input-field ">
                                                <select id="role_id_user" name="role">
                                                    <option value="" disabled selected>Elige un rol*</option>
                                                    <?php foreach ($rol['roles'] as $r) : ?>
                                                        <option value="<?php echo $r['id']; ?>"><?php echo $r['name'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- botn de enviar -->
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Agregar</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <script src="assets/js/edits_ajax/edit_user.js"></script>

    <!-- Custom Js -->
    <script src="assets/js/pages/ui/dialogs.js"></script>
    <!-- Export table -->
    <script src="assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="assets/js/pages/apps/support.js"></script>

<?php else : Core::redir("index.php?view=home");
endif ?>