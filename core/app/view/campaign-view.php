<section class="content">
    <div class="container-fluid">

        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style">
                        <li class="breadcrumb-item 	bcrumb-1">
                            <a href="./index.php?view=home"><i class="material-icons">home</i>Inicio</a>
                        </li>
                        <li class="breadcrumb-item active">Campaña</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <!-- Single Slide -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">

                    <div class="header">
                        <h2>
                            <strong>Campañas</strong> Disponibles
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="#" onClick="return false;">Action</a>
                                    </li>
                                    <li>
                                        <a href="#" onClick="return false;">Another action</a>
                                    </li>
                                    <li>
                                        <a href="#" onClick="return false;">Something else here</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div class="body">
                        <div class="owl-carousel owl-theme" id="single_slide">
                            <div class="item">
                                <img src="assets/images/image-gallery/1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/image-gallery/2.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/image-gallery/3.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/image-gallery/4.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/image-gallery/5.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Single Slide -->
        </div>


    </div>
</section>

<!-- Demo Js -->
<script src="assets/js/pages/medias/carousel.js"></script>