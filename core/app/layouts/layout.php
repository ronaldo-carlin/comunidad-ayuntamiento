<?php
header('Access-Control-Allow-Origin: *');
date_default_timezone_set('America/Mexico_City');

if (isset($_SESSION["user_id"])) :
    /* echo '<pre>';
    print_r($_SESSION["user_id"]);
    echo '</pre>'; */
    $u = UserData::getUserByIdAPI($_SESSION["user_id"]);

    foreach ($u['user'] as $a) {
        $id = $a['id'];
        $rol_id = $a['rol_id'];
        $img = $a['img'];
    }
?>

    <!DOCTYPE html>
    <html lang="en" style="background-color: #38424b;">

    <head>
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">

        <!-- Title -->
        <title>ADMIN | Mixtequilla</title>

        <!-- Favicon and Touch Icons  -->
        <link rel="shortcut icon" href="assets/images/favicon.png"> <!-- icono web -->
        <link rel="apple-touch-icon-precomposed" href="assets/images/favicon.png"> <!-- icono dispositivo movil -->
        <meta name="msapplication-TileImage" content="assets/images/favicon.png">

        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <meta property="site_name" content="Ignacio de la llave | Mixtequilla"> <!-- Nombre del Sitio -->
        <meta property="title" content="Ignacio de la llave | Mixtequilla" /> <!-- Titulo o nombre -->
        <meta name="description" content="Mixtequilla">

        <meta property="url" content="https://ignaciodelallave.gob.mx" /> <!-- URL -->
        <meta property="locale" content="es_MX" /> <!-- Localidad -->
        <meta property="language" content="ES" /> <!-- Lenguaje -->

        <meta name="keywords" content="agency, agency Portfolio, agency template, creative agency, creative business, creative multipurpose, creative Portfolio, creative html, digital, marketing agency, modern business, multipurpose, Portfolio showcase, professional website,"> <!-- Descripción -->
        <meta name="author" content="https://github.com/ronaldo-carlin"> <!-- creador de la pagina -->

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="theme-color" content="#890000"> <!-- Color de Navegador -->

        <!-- SEO -->
        <link rel="dns-prefetch" href="//www.google.com">
        <link rel="dns-prefetch" href="//s.w.org">

        <!-- Fontawesome -->
        <link href="assets/fontawesome/css/all.min.css" rel="stylesheet">
        <link href="assets/fontawesome/css/fontawesome.min.css" rel="stylesheet">

        <!-- Colorpicker Css -->
        <link href="assets/js/bundles/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css" rel="stylesheet" />

        <!-- Multi Select Css -->
        <link href="assets/js/bundles/multiselect/css/multi-select.css" rel="stylesheet">

        <!-- style general -->

        <!-- Plugins Core Css -->
        <link href="assets/css/app.min.css" rel="stylesheet">
        <link href="assets/css/form.min.css" rel="stylesheet">
        <!-- Custom Css -->
        <link href="assets/css/style.css" rel="stylesheet" />
        <!-- Theme style. You can choose a theme from css/themes instead of get all themes -->
        <link href="assets/css/styles/all-themes.css" rel="stylesheet" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="assets/js/ckeditor5/ckeditor.js"></script>
        <script src="assets/js/bundles/ckeditor/ckeditor.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script src="https://unpkg.com/@popperjs/core@2"></script>
        <script src="https://unpkg.com/tippy.js@6"></script>

    </head>

    <body class="dark">

        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30">
                    <img class="loading-img-spin" src="assets/images/loading.png" alt="admin">
                </div>
                <p>Cargando...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->

        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container-fluid">

                <div class="navbar-header">
                    <a href="#" onClick="return false;" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="#" onClick="return false;" class="bars"></a>
                    <a class="navbar-brand" href="./index.php?view=home">
                        <img id="" src="assets/images/icono.png" alt="" width="40px" />
                        <!-- <span class="logo-name"><img src="assets/images/icono.png" alt="" width="40px"/></span> -->
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" onClick="return false;" class="sidemenu-collapse">
                                <i class="nav-hdr-btn ti-align-left"></i>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">

                        <!-- Full Screen Button -->
                        <li class="fullscreen">
                            <a href="javascript:;" class="fullscreen-btn">
                                <i class="nav-hdr-btn ti-fullscreen"></i>
                            </a>
                        </li>
                        <!-- #END# Full Screen Button -->

                        <!-- Notifications-->
                        <li class="dropdown">
                            <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="nav-hdr-btn ti-bell"></i>
                                <span class="notify"></span>
                                <span class="heartbeat"></span>
                            </a>

                            <ul class="dropdown-menu pullDown">
                                <li class="header">NOTIFICATIONS</li>
                                <li class="body">
                                    <ul class="menu">
                                        <li>
                                            <a href="#" onClick="return false;">
                                                <span class="table-img msg-user">
                                                    <img src="assets/images/user/user.png" alt="">
                                                </span>
                                                <span class="menu-info">
                                                    <span class="menu-title">Sarah Smith</span>
                                                    <span class="menu-desc">
                                                        <i class="material-icons">access_time</i> 14 mins ago
                                                    </span>
                                                    <span class="menu-desc">Please check your email.</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">
                                                <span class="table-img msg-user">
                                                    <img src="assets/images/user/user2.jpg" alt="">
                                                </span>
                                                <span class="menu-info">
                                                    <span class="menu-title">Airi Satou</span>
                                                    <span class="menu-desc">
                                                        <i class="material-icons">access_time</i> 22 mins ago
                                                    </span>
                                                    <span class="menu-desc">Please check your email.</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">
                                                <span class="table-img msg-user">
                                                    <img src="assets/images/user/user3.jpg" alt="">
                                                </span>
                                                <span class="menu-info">
                                                    <span class="menu-title">John Doe</span>
                                                    <span class="menu-desc">
                                                        <i class="material-icons">access_time</i> 3 hours ago
                                                    </span>
                                                    <span class="menu-desc">Please check your email.</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">
                                                <span class="table-img msg-user">
                                                    <img src="assets/images/user/user4.jpg" alt="">
                                                </span>
                                                <span class="menu-info">
                                                    <span class="menu-title">Ashton Cox</span>
                                                    <span class="menu-desc">
                                                        <i class="material-icons">access_time</i> 2 hours ago
                                                    </span>
                                                    <span class="menu-desc">Please check your email.</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">
                                                <span class="table-img msg-user">
                                                    <img src="assets/images/user/user5.jpg" alt="">
                                                </span>
                                                <span class="menu-info">
                                                    <span class="menu-title">Cara Stevens</span>
                                                    <span class="menu-desc">
                                                        <i class="material-icons">access_time</i> 4 hours ago
                                                    </span>
                                                    <span class="menu-desc">Please check your email.</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">
                                                <span class="table-img msg-user">
                                                    <img src="assets/images/user/user6.jpg" alt="">
                                                </span>
                                                <span class="menu-info">
                                                    <span class="menu-title">Charde Marshall</span>
                                                    <span class="menu-desc">
                                                        <i class="material-icons">access_time</i> 3 hours ago
                                                    </span>
                                                    <span class="menu-desc">Please check your email.</span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">
                                                <span class="table-img msg-user">
                                                    <img src="assets/images/user/user7.jpg" alt="">
                                                </span>
                                                <span class="menu-info">
                                                    <span class="menu-title">John Doe</span>
                                                    <span class="menu-desc">
                                                        <i class="material-icons">access_time</i> Yesterday
                                                    </span>
                                                    <span class="menu-desc">Please check your email.</span>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer">
                                    <a href="#" onClick="return false;">View All Notifications</a>
                                </li>
                            </ul>
                        </li>
                        <!-- #END# Notifications-->

                        <!-- user icon -->
                        <li class="dropdown user_profile">
                            <div class="dropdown-toggle" data-toggle="dropdown">
                                <div style="width: 3rem;">
                                    <!-- <img src="assets/images/user/user.png" alt="user"> -->
                                    <img alt="portada" id="profile_img" style="width: 100px; display: none;">
                                </div>
                            </div>

                            <ul class="dropdown-menu pullDown">
                                <li class="body">
                                    <ul class="user_dw_menu">
                                        <li>
                                            <!-- <a href="./index.php?view=perfil" onClick="return false;"> -->
                                            <a href="./index.php?view=perfil">
                                                <i class="material-icons">person</i>Perfil
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">
                                                <i class="material-icons">feedback</i>Retroalimentaciones
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">
                                                <i class="material-icons">help</i>Ayuda
                                            </a>
                                        </li>
                                        <li>
                                            <a href="logout.php">
                                                <i class="material-icons">power_settings_new</i>Cerrar Sesión
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- #END# Tasks -->

                        <li class="pull-right">
                            <a href="#" onClick="return false;" class="js-right-sidebar" data-close="true">
                                <i class="nav-hdr-btn ti-layout-grid2"></i>
                            </a>
                        </li>

                    </ul>
                </div>

            </div>
        </nav>
        <!-- #Top Bar -->

        <!-- menu -->
        <div>
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">

                <!-- Menu -->
                <div class="menu">
                    <ul class="list">

                        <!-- user -->
                        <li>
                            <div class="sidebar-profile clearfix d-flex align-items-center">
                                <div class="profile-img">
                                    <img alt="portada" id="profile_img2" style="width: 100px; display: none;">
                                </div>
                                <div class="profile-info">
                                    <p class="m-0">Bienvenido!</p>
                                    <h3 id="name_user"></h3>
                                </div>
                            </div>
                        </li>
                        <!-- end user -->

                        <!-- start menu -->
                        <li class="header">-- Main</li>

                        <!-- inicio- -->
                        <li id="li-home">
                            <a href="./index.php?view=home">
                                <i class="menu-icon ti-home"></i>
                                <span>Home</span>
                            </a>
                            <!-- <ul class="ml-menu">
                            <li class="active">
                                <a href="./index.php?view=home">Dashboard 1</a>
                            </li>
                            <li>
                                <a href="pages/dashboard/dashboard2.html">Dashboard 2</a>
                            </li>
                            <li>
                                <a href="pages/dashboard/dashboard3.html">Dashboard 3</a>
                            </li>
                        </ul> -->
                        </li>

                        <!-- calendario -->
                        <li id="li-calendario">
                            <a href="./index.php?view=calendario">
                                <i class="menu-icon ti-calendar"></i>
                                <span>Calendario</span>
                            </a>
                        </li>

                        <!-- campañas -->
                        <li id="li-campanas">
                            <a href="./index.php?view=campaign">
                                <i class="menu-icon ti-announcement"></i>
                                <span>Campañas</span>
                            </a>
                        </li>

                        <!-- covid-19 -->
                        <li id="li-covid">
                            <a href="./index.php?view=covid">
                                <i class="menu-icon ti-alert"></i>
                                <span>COVID-19</span>
                            </a>
                        </li>

                        <!-- eventos -->
                        <li id="li-eventos">
                            <a href="./index.php?view=eventos">
                                <i class="menu-icon ti-clipboard"></i>
                                <span>Eventos</span>
                            </a>
                        </li>

                        <!-- directorio -->
                        <li id="li-directorio">
                            <a href="./index.php?view=directorio">
                                <i class="menu-icon far fa-address-book"></i>
                                <span>Directorio</span>
                            </a>
                        </li>

                        <!-- noticias -->
                        <li id="li-noticias">
                            <a href="./index.php?view=noticias">
                                <i class="menu-icon far fa-newspaper"></i>
                                <span>Noticias</span>
                            </a>
                        </li>

                        <?php if ($rol_id == 1 || $rol_id == 5) : ?>
                            <li class="header">-- Tramites</li>
                            <!-- predial -->
                            <li id="li-predial">
                                <a href="./index.php?view=predial">
                                    <i class="menu-icon far fa-money-bill-alt"></i>
                                    <span>Predial</span>
                                </a>
                            </li>
                            <li class="header">-- Servicios</li>
                            <!-- atencion ciudadana -->
                            <li id="li-atencion-ciudadana">
                                <a href="./index.php?view=atencion_ciudadana">
                                    <i class="menu-icon fas fa-child"></i>
                                    <span>Atención Ciudadana</span>
                                </a>
                            </li>
                            <!-- atencion a migrantes -->
                            <li id="li-atencion_migrantes">
                                <a href="./index.php?view=atencion_migrantes">
                                    <i class="menu-icon fas fa-hands-helping"></i>
                                    <span>Atención a Migrantes</span>
                                </a>
                            </li>
                            <!-- aviso de privacidad -->
                            <li id="li-aviso_privacidad">
                                <a href="./index.php?view=aviso_privacidad">
                                    <i class="menu-icon fas fa-lock"></i>
                                    <span>Aviso de Privacidad</span>
                                </a>
                            </li>
                            <!-- cercano a ti! -->
                            <li id="li-cercano_a_ti">
                                <a href="./index.php?view=cercano_a_ti">
                                    <i class="menu-icon fas fa-hand-holding-heart"></i>
                                    <span>Cercano a ti!</span>
                                </a>
                            </li>
                            <li class="header">-- Publicaciones</li>
                            <!-- calendario config -->
                            <li id="li-calendario_admin">
                                <a href="./index.php?view=calendario_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Calendario</span>
                                </a>
                            </li>
                            <!-- campañas config -->
                            <li id="li-campanas_admin">
                                <a href="./index.php?view=campaign_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Campañas</span>
                                </a>
                            </li>
                            <!-- covid-19 config -->
                            <li id="li-covid_admin">
                                <a href="./index.php?view=covid_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>COVID-19</span>
                                </a>
                            </li>
                            <!-- eventos config -->
                            <li id="li-eventos_admin">
                                <a href="./index.php?view=eventos_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Eventos</span>
                                </a>
                            </li>
                            <!-- noticias config -->
                            <li id="li-noticias_admin">
                                <a href="./index.php?view=noticias_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Noticias</span>
                                </a>
                            </li>
                            <!-- transparencia config -->
                            <li id="li-transparencia">
                                <a href="./index.php?view=transparencia">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Transparencia</span>
                                </a>
                            </li>
                            <li class="header">-- Administración</li>
                            <!-- administracion -->
                            <li id="li-administracion">
                                <a href="./index.php?view=administracion_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Administración</span>
                                </a>
                            </li>
                            <!-- areas -->
                            <li id="li-areas">
                                <a href="./index.php?view=areas_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Areas</span>
                                </a>
                            </li>
                            <!-- departamentos -->
                            <li id="li-departamento">
                                <a href="./index.php?view=departamento_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Departamentos</span>
                                </a>
                            </li>
                            <!-- directorio config -->
                            <li id="li-directorio_admin">
                                <a href="./index.php?view=directorio_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Directorio</span>
                                </a>
                            </li>
                            <!-- usuarios config -->
                            <li id="li-usuarios">
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="menu-icon fas fa-users-cog"></i>
                                    <span>Usuarios</span>
                                </a>
                                <ul class="ml-menu">
                                    <li id="li-empleados">
                                        <a href="./index.php?view=empleados_admin">
                                            <i class="menu-icon ti-settings"></i>
                                            Empleados
                                        </a>
                                    </li>
                                    <li id="li-usuarios">
                                        <a href="./index.php?view=usuarios">
                                            <i class="menu-icon ti-settings"></i>
                                            Usuarios
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end menu -->
                        <?php endif; ?>

                        <?php if ($rol_id == 6) : ?>
                            <li class="header">-- Publicaciones</li>
                            <!-- calendario config -->
                            <li id="li-calendario_admin">
                                <a href="./index.php?view=calendario_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Calendario</span>
                                </a>
                            </li>
                            <!-- campañas config -->
                            <li id="li-campanas_admin">
                                <a href="./index.php?view=campaign_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Campañas</span>
                                </a>
                            </li>
                            <!-- covid-19 config -->
                            <li id="li-covid_admin">
                                <a href="./index.php?view=covid_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>COVID-19</span>
                                </a>
                            </li>
                            <!-- eventos config -->
                            <li id="li-eventos_admin">
                                <a href="./index.php?view=eventos_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Eventos</span>
                                </a>
                            </li>
                            <!-- noticias config -->
                            <li id="li-noticias_admin">
                                <a href="./index.php?view=noticias_admin">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Noticias</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if ($rol_id == 10) : ?>
                            <li class="header">-- Publicaciones</li>
                            <!-- transparencia config -->
                            <li id="li-transparencia">
                                <a href="./index.php?view=transparencia">
                                    <i class="menu-icon ti-settings"></i>
                                    <span>Transparencia</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if ($rol_id == 8) : ?>
                            <li class="header">-- Servicios</li>
                            <!-- atencion ciudadana -->
                            <li id="li-atencion-ciudadana">
                                <a href="./index.php?view=atencion_ciudadana">
                                    <i class="menu-icon fas fa-child"></i>
                                    <span>Atención Ciudadana</span>
                                </a>
                            </li>
                            <!-- atencion a migrantes -->
                            <li id="li-atencion_migrantes">
                                <a href="./index.php?view=atencion_migrantes">
                                    <i class="menu-icon fas fa-hands-helping"></i>
                                    <span>Atención a Migrantes</span>
                                </a>
                            </li>
                            <!-- aviso de privacidad -->
                            <li id="li-aviso_privacidad">
                                <a href="./index.php?view=aviso_privacidad">
                                    <i class="menu-icon fas fa-lock"></i>
                                    <span>Aviso de Privacidad</span>
                                </a>
                            </li>
                            <!-- cercano a ti! -->
                            <li id="li-cercano_a_ti">
                                <a href="./index.php?view=cercano_a_ti">
                                    <i class="menu-icon fas fa-hand-holding-heart"></i>
                                    <span>Cercano a ti!</span>
                                </a>
                            </li>
                        <?php endif; ?>

                    </ul>
                </div>
                <!-- #Menu -->

            </aside>
            <!-- #END# Left Sidebar -->

            <!-- Right Sidebar -->
            <aside id="rightsidebar" class="right-sidebar">

                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation">
                        <a href="#skins" data-toggle="tab" class="active">SKINS</a>
                    </li>
                    <!-- <li role="presentation">
                    <a href="#settings" data-toggle="tab">SETTINGS</a>
                </li> -->
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane in active in active stretchLeft" id="skins">
                        <div class="demo-skin">

                            <!-- <div class="rightSetting">
                            <p>GENERAL SETTINGS</p>
                            <ul class="setting-list list-unstyled m-t-20">
                                <li>
                                    <div class="form-check">
                                        <div class="form-check m-l-10">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="" checked> Save
                                                History
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <div class="form-check m-l-10">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="" checked> Show
                                                Status
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <div class="form-check m-l-10">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="" checked> Auto
                                                Submit Issue
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <div class="form-check m-l-10">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="" checked> Show
                                                Status To All
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div> -->

                            <div class="rightSetting">
                                <p>COLORES DEL MENÚ DE LA BARRA LATERAL</p>
                                <button type="button" class="btn btn-sidebar-light btn-border-radius p-l-20 p-r-20">Light</button>
                                <button type="button" class="btn btn-sidebar-dark btn-default btn-border-radius p-l-20 p-r-20">Dark</button>
                            </div>

                            <div class="rightSetting">
                                <p>COLORES DEL TEMA</p>
                                <button type="button" class="btn btn-theme-light btn-border-radius p-l-20 p-r-20">Light</button>
                                <button type="button" class="btn btn-theme-dark btn-default btn-border-radius p-l-20 p-r-20">Dark</button>
                            </div>

                            <div class="rightSetting">
                                <p>COLORES</p>
                                <ul class="demo-choose-skin choose-theme list-unstyled">
                                    <li data-theme="black" class="actived">
                                        <div class="black-theme"></div>
                                    </li>
                                    <li data-theme="white">
                                        <div class="white-theme white-theme-border"></div>
                                    </li>
                                    <li data-theme="purple">
                                        <div class="purple-theme"></div>
                                    </li>
                                    <li data-theme="blue">
                                        <div class="blue-theme"></div>
                                    </li>
                                    <li data-theme="cyan">
                                        <div class="cyan-theme"></div>
                                    </li>
                                    <li data-theme="green">
                                        <div class="green-theme"></div>
                                    </li>
                                    <li data-theme="orange">
                                        <div class="orange-theme"></div>
                                    </li>
                                </ul>
                            </div>

                            <!-- <div class="rightSetting">
                            <p>Disk Space</p>
                            <div class="sidebar-progress">
                                <div class="progress m-t-20">
                                    <div class="progress-bar l-bg-cyan shadow-style width-per-45" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="progress-description">
                                    <small>26% remaining</small>
                                </span>
                            </div>
                        </div>

                        <div class="rightSetting m-b-15">
                            <p>Server Load</p>
                            <div class="sidebar-progress">
                                <div class="progress m-t-20">
                                    <div class="progress-bar l-bg-orange shadow-style width-per-63" role="progressbar" aria-valuenow="63" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="progress-description">
                                    <small>Highly Loaded</small>
                                </span>
                            </div>
                        </div> -->

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane stretchRight" id="settings">
                        <div class="demo-settings">
                            <p>GENERAL SETTINGS</p>
                            <ul class="setting-list">
                                <li>
                                    <span>Report Panel Usage</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" checked>
                                            <span class="lever switch-col-green"></span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <span>Email Redirect</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox">
                                            <span class="lever switch-col-blue"></span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                            <p>SYSTEM SETTINGS</p>
                            <ul class="setting-list">
                                <li>
                                    <span>Notifications</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" checked>
                                            <span class="lever switch-col-purple"></span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <span>Auto Updates</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" checked>
                                            <span class="lever switch-col-cyan"></span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                            <p>ACCOUNT SETTINGS</p>
                            <ul class="setting-list">
                                <li>
                                    <span>Offline</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" checked>
                                            <span class="lever switch-col-red"></span>
                                        </label>
                                    </div>
                                </li>
                                <li>
                                    <span>Location Permission</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox">
                                            <span class="lever switch-col-lime"></span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- #END# Right Sidebar -->
        </div>
        <!-- #menu -->


        <div>
            <?php View::load("index") ?>
        </div>

        <script src="assets/js/app.min.js"></script>
        <script src="assets/js/chart.min.js"></script>

        <!-- Custom Js -->
        <script src="assets/js/admin.js"></script>
        <script src="assets/js/bundles/echart/echarts.js"></script>
        <script src="assets/js/bundles/apexcharts/apexcharts.min.js"></script>
        <script src="assets/js/pages/index.js"></script>

        <script src='https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js'></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="assets/js/pages/charts/jquery-knob.js"></script>
        <script src="https://cdn.jsdelivr.net/gh/moment/moment@develop/min/moment-with-locales.min.js"></script>
        <script src="assets/js/pages/forms/form-validation.js"></script>
        <script src="assets/js/form.min.js"></script>
        <script src="assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
        <script src="assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
        <script src="assets/js/pages/forms/advanced-form-elements.js"></script>
        <script src="assets/js/pages/apps/portfolio.js"></script>
        <script src="assets/js/pages/apps/calendar.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="assets/js/bundles/amcharts4/core.js"></script>
        <script src="assets/js/bundles/amcharts4/charts.js"></script>
        <script src="assets/js/bundles/amcharts4/animated.js"></script>
        <script src="assets/js/bundles/lightgallery/dist/js/lightgallery-all.js"></script>
        <script src="assets/js/pages/medias/image-gallery.js"></script>
        <script src="assets/js/pages/forms/basic-form-elements.js"></script>
        <script src="https://cdnjs.com/libraries/moment.js"> </script>
        <script src="assets/js/pages/apps/chat.js"></script>
        <script src="assets/js/pages/ui/sweet.js"></script>
        <script src="assets/js/pages/todo/todo.js"></script>
        <script src="assets/js/pages/widgets/data-widget.js"></script>
        <script src="assets/js/pages/forms/editors.js"></script>
        <!--    <script src="assets/js/bundles/ckeditor/ckeditor.js"></script> -->
        <!--     <script src="assets/js/bundles/tinymce/tinymce.min.js"></script> -->
        <!-- Custom Js -->

        <!-- Datatable Js -->
        <script src="assets/js/table.min.js"></script>
        <script src="assets/js/bundles/export-tables/dataTables.buttons.min.js"></script>
        <script src="assets/js/bundles/export-tables/buttons.flash.min.js"></script>
        <script src="assets/js/bundles/export-tables/jszip.min.js"></script>
        <script src="assets/js/bundles/export-tables/vfs_fonts.js"></script>
        <script src="assets/js/bundles/export-tables/buttons.html5.min.js"></script>
        <script src="assets/js/bundles/export-tables/buttons.print.min.js"></script>
        <script src="assets/js/pages/tables/jquery-datatable.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale-all.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>

        <!-- Carousel Js -->
        <script src="assets/js/pages/medias/carousel.js"></script>

        <input type="text" hidden id="id_user_layout" value="<?php echo $id ?>">

        <script>
            $(document).ready(function() {
                let id = $("#id_user_layout").val();

                //console.log(id);

                $data = {
                    opt: "id_user",
                    id: id,
                };

                //consulta para traer el ususrio por id
                envioAjax($data, "./index.php?action=ajaxrequest")
                    .done((response) => {
                        var data = response["user"];
                        //console.log(data);
                        //console.log(response.user[0].img);

                        if (data.length > 0) {

                            if (data[0].img == '' | data[0].img == null) {
                                $("#profile_img").attr("src", 'files/imgUsers/user.png');
                                $("#profile_img2").attr("src", 'files/imgUsers/user.png');
                            } else {
                                $("#profile_img").attr("src", 'files/imgUsers/' + data[0].img);
                                $("#profile_img2").attr("src", 'files/imgUsers/' + data[0].img);
                            }

                            document.getElementById("profile_img").style.display = "block";
                            document.getElementById("profile_img2").style.display = "block";

                            $("#name_user").text(data[0].name);

                        }
                    })
                    .fail((jqXHR) => {
                        console.log(jqXHR);
                    });
            });

            function envioAjax(datas = [], url) {
                var jqXHR = $.ajax({
                    url: url,
                    dataType: "JSON",
                    type: "POST",
                    data: datas,
                });

                return jqXHR;
            }
        </script>

    </body>

    </html>
<?php else : ?>
    <!-- LOGIN -->
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <title>LOGIN | Mixtequilla</title>
        <!-- Favicon-->
        <link rel="icon" href="assets/images/favicon.png" type="image/x-icon">

        <link href="assets/css/pages/extra_pages.css" rel="stylesheet">
        <link href="assets/css/login.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    </head>

    <body>

        <img class="wave" src="assets/images/login/wave.png">

        <div class="container">

            <div class="img">
                <img src="assets/images/login/bg.png">
            </div>

            <div class="login-content">
                <form id="form_validation" action="./?action=processlogin" enctype="multipart/form-data" method="POST">

                    <img src="assets/images/login/avatar verde.png">
                    <h2 class="title">bienvenidos!</h2>

                    <div class="input-div one validate-input" data-validate="Valid email is required: ex@abc.xyz" <div class="i">
                        <div class="i">
                            <i class="fas fa-user"></i>
                        </div>
                        <div class="div">
                            <h5>Usuario</h5>
                            <input type="email" class="input" name="email">
                        </div>
                    </div>

                    <div class="input-div pass validate-input" data-validate="Password is required">
                        <div class="i">
                            <i class="fas fa-lock"></i>
                        </div>
                        <div class="div">
                            <h5>Contraseña</h5>
                            <input type="password" class="input" name="password">
                        </div>
                    </div>

                    <a href="forgotpassword.php">¿Has olvidado tu contraseña?</a>

                    <?php if (isset($_GET["status"]) == 'error') : ?>
                        <div class="alert alert-danger">
                            <strong>Alerta!</strong> Usuario o Contraseña Incorrecta, Intenta de nuevo.
                        </div>
                    <?php endif; ?>

                    <input type="submit" class="btn btn-hover btn-border-radius color-8" value="Aceptar">

                </form>
            </div>
        </div>

        <!-- login -->
        <script type="text/javascript" src="assets/js/login.js"></script>

        <!-- Plugins Js -->
        <script src="assets-admin/js/app.min.js"></script>
        <!-- Extra page Js -->
        <script src="assets-admin/js/pages/examples/pages.js"></script>

    </body>

    </html>
<?php endif; ?>