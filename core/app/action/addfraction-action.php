<?php
/* echo '<pre>';
print_r($_POST);
echo '</pre>'; */

$u   = UserData::getById($_SESSION['user_id']);
$entity =  new FractionData();

foreach ($_POST as $k => $v) {
    $entity->$k = $v;
    # code...
}

$handle = new Upload($_FILES['filename']);
if ($handle->uploaded) {
    $url = "./files/iconFraction/";
    $handle->Process($url);
    $entity->filename = $handle->file_dst_name;
}


$entity->created_by = $_SESSION["user_id"];
$entity->role_id = $_SESSION["rol_id"];

/* echo '<pre>';
print_r($entity);
echo '</pre>'; */

$entity->addAPI();
Core::redir("index.php?view=transparencia");