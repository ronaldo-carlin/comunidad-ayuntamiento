<?php

// define('LBROOT',getcwd()); // LegoBox Root ... the server root
// include("core/controller/Database.php");

if (!isset($_SESSION["user_id"])) {
	$email = $_POST['email'];
	//$password = $_POST['password'];
	$password = sha1(md5($_POST['password']));

	$userData = UserData::loginAPI($email, $password);

	//print_r($userData);

	$base = new Database();
	$con = $base->connect();

	$userid = $userData['user'][0]['id'];
	$rol_id = $userData['user'][0]['rol_id'];
	$department_id = $userData['user'][0]['department_id'];
	$administration_id = $userData['user'][0]['administration_id'];
	$updated_password = $userData['user'][0]['updated_password_at'];

	if ($userData['success'] == true) {

		if (!isset($_SESSION)) {
			session_start();
		}

		//	print $userid;
		$_SESSION['user_id'] = $userid;
		$_SESSION['rol_id'] = $rol_id;
		$_SESSION['administration_id'] = $administration_id;
		$_SESSION['department_id'] = $department_id;

		//	setcookie('userid',$userid);
		//	print $_SESSION['userid'];

		// print_r($_POST);

		$entity =  new ReportLoginData();
		foreach ($_POST as $k => $v) {
			$entity->$k = trim($v);
			# code...
		}

		$entity->id_user = $userid;
		$entity->add();
		//print "Cargando ... $email";
		print ' 
		<head>
		<!-- Custom Css -->
        <link href="assets/css/style.css" rel="stylesheet" />
        <!-- Theme style. You can choose a theme from css/themes instead of get all themes -->
        <link href="assets/css/styles/all-themes.css" rel="stylesheet" />
		</head>
		<div class="page-loader-wrapper d-flex justify-content-center align-items-center">
            <div class="loader">
                <div class="m-t-30">
					<img class="loading-img-spin" src="assets/images/loading.png" alt="admin">
                </div>
                <p>Espere un momento...</p>
            </div>
        </div>';

		if ($updated_password == null || $updated_password == '' || $updated_password == '0000-00-00 00:00:00') {

			if (isset($_SESSION['user_id'])) {
				unset($_SESSION['user_id']);
			}
			session_destroy();

			$token1 = bin2hex(random_bytes(8));
			$token2 = bin2hex(random_bytes(8));
			print "<script>window.location='changepassnew.php?id=" . $token1 . $userid . $token2 . "';</script>";
		} else {
			if ($rol_id == 1) {
				print "<script>window.location='index.php?view=usuarios';</script>";
				//print_r('el rol es igual a 1');
			} elseif ($rol_id == 10) {
				print "<script>window.location='index.php?view=transparencia';</script>";
			} elseif ($rol_id == 6) {
				print "<script>window.location='index.php?view=noticias_admin';</script>";
			} else{
				print "<script>window.location='index.php?view=home';</script>";
				//print_r('el rol es igual a 0');
			}
		}
	} else {
		print "<script>window.location='index.php?view=login&status=error';</script>";
		//print_r('ya esta logiado');
		/* echo '<pre>';
		print_r($userData);
		echo '</pre>'; */
	}
} else {
	print "<script>window.location='index.php?view=login';</script>";
	//print_r('no funciona');
}
