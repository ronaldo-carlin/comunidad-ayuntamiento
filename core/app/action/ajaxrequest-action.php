<?php
$data = array();
header('Content-type: application/json; charset=utf-8');

if ($_POST['opt'] == 'autcompletado') {
     $users = UserData::getLike($_POST['term']);
     $option = '';
     foreach ($users as $value) {
          $option .= '<div><a class="suggest-element" data-name="' . ($value->name) . '" id="input' . $value->id . '" data-id="' . $value->id . '">' . ($value->name) . '</a></div>';
     }
     $data = $option;
     /*     $data['data'] = $users;
     $data['post'] = $_POST; */
}
if ($_POST['opt'] == 'codigo') {

     $company_id = $_POST['company_id'];
     $position_id = $_POST['position_id'];

     $data = UserData::getEvalByCompanyIdAndCodeId($company_id, $position_id);
}
if ($_POST['opt'] == 'addsalary') {

     $user_id = $_POST['user_id'];
     $salary_day = $_POST['salary_day'];
     $salary_month = $_POST['salary_month'];
     $bonus_day = $_POST['bonus_day'];
     $days =  $_POST['days'];

     $data = UserData::updateSalary($user_id, $salary_day, $salary_month, $bonus_day, $days);
}
if ($_POST['opt'] == 'showConva') {

     $show = $_POST['show'];
     $user_id = $_POST['user_id'];

     $data = UserData::showCova($user_id, $show);
}
if ($_POST['opt'] == 'closeEvaluation') {

     $evaluation_id = $_POST['id'];
     $data = EvaluationData::closeEvaluation($evaluation_id);
}
if ($_POST['opt'] == 'empleados') {

     $empresa_id = $_POST['empresa_id'];
     $data = UserData::getUsersByCompanyAPI($empresa_id);
}
if ($_POST['opt'] == 'editquestion') {

     $id_question = $_POST['id_question'];

     $data = QuestionData::getById($id_question);
}
if ($_POST['opt'] == 'editEvaluation') {

     $idEvaluation = $_POST['idEvaluation'];

     $data = EvaluationData::getById($idEvaluation);
}
if ($_POST['opt'] == 'editperiod') {

     $id_period = $_POST['id_period'];

     $data = PeriodData::getById($id_period);
}
if ($_POST['opt'] == 'editcompetence') {

     $id_competence = $_POST['id_competence'];

     $data = CompetenceData::getById($id_competence);
}
if ($_POST['opt'] == 'edituser') {

     $user_id = $_POST['user_id'];

     $data = UserData::getById($user_id);
}
if ($_POST['opt'] == 'infoquestion') {

     $info_competence = $_POST['info_competence'];

     $data = QuestionData::getById($info_competence);
}
if ($_POST['opt'] == 'infocompetence') {

     $info_competence = $_POST['info_competence'];

     $data = CompetenceData::getById($info_competence);
}
if ($_POST['opt'] == 'editposition') {

     $id_position = $_POST['id_position'];

     $data = PositionData::getById($id_position);
}
if ($_POST['opt'] == 'relaciones') {

     $data = RelationData::getAll();
}

//? Comunidad
if ($_POST['opt'] == 'releases') {

     $id = $_POST['id'];

     $data = ReleaseData::getByIdAPI($id);
}
if ($_POST['opt'] == 'news') {

     $id = $_POST['id'];

     $data = NewsData::getByIdAPI($id);
}
if ($_POST['opt'] == 'promotions') {

     $id = $_POST['id'];

     $data = PromotionsData::getByIdAPI($id);
}
if ($_POST['opt'] == 'quality') {

     $id = $_POST['id'];

     $data = QualityData::getQualityByIdAPI($id);
}
if ($_POST['opt'] == 'process') {

     $id = $_POST['id'];

     $data = ProcessData::getByIdAPI($id);
}
if ($_POST['opt'] == 'users') {

     $data = UserData::getAll();
}
if ($_POST['opt'] == 'permissions') {
     $id = $_POST['id'];
     //$data = RolessettingsData::getSettingsByRoleId($id);
}
if ($_POST['opt'] == 'events') {
     $id = $_POST['id'];
     $data = CalendarData::getEventByIdAPI($id);
}
if ($_POST['opt'] == 'format') {
     $id = $_POST['id'];
     $data = FormatData::getFormatByid($id);
}
if ($_POST['opt'] == 'calendar') {
     $company_id = UserData::getById($_SESSION["user_id"])->company_id;
     $dates = CalendarData::getEventByCompanyAPI($company_id);

     foreach ($dates['calendar'] as $key => $value) {
          $data[$key]['id'] = $value['id'];
          $data[$key]['title'] = $value['name'];
          $data[$key]['description'] = $value['name'];
          $data[$key]['start'] = date('Y-m-d', strtotime($value['date_start']));
          //$data[$key]['end'] = date('Y-m-d', strtotime($value->date_end));
          if ($value['repeat_yearly'] == 1) {
               $data[$key]['repeat'] = 2;
          }
          $data[$key]['className'] = 'scheduler_basic_event';
          if ($value['event_id'] == 1) {
               $data[$key]['backgroundColor'] = "#8ac926";
          } elseif ($value['event_id'] == 2) {
               $data[$key]['backgroundColor'] = "#1982c4";
          } elseif ($value['event_id'] == 3) {
               $data[$key]['backgroundColor'] = "#ffca3a";
          } elseif ($value['event_id'] == 4) {
               $data[$key]['backgroundColor'] = "#c1121f";
          } elseif ($value['event_id'] == 5) {
               $data[$key]['backgroundColor'] = "#7400b8";
          } elseif ($value['event_id'] == 6) {
               $data[$key]['backgroundColor'] = "#353535";
          } elseif ($value['event_id'] == 7) {
               $data[$key]['backgroundColor'] = "#7f5539";
          }
     }
}


if ($_POST['opt'] == 'id_user') {

     $id = $_POST['id'];

     $data = UserData::getUserByIdAPI($id);
}
if ($_POST['opt'] == 'update_user') {

     $user_id = $_POST['user_id'];

     $data = UserData::getUserByIdAPI($user_id);
}
if ($_POST['opt'] == 'fraction') {

     $fraction_id = $_POST['id'];

     $data = FractionData::getFractionById($fraction_id);
}
if ($_POST['opt'] == 'sections') {

     $fraction_id = $_POST['id'];

     $data = SectionData::getAllByFraction($fraction_id);
}
if ($_POST['opt'] == 'sections_id') {

     $fraction_id = $_POST['id'];

     $data = SectionData::getSectionById($fraction_id);
}
if ($_POST['opt'] == 'period') {

     $section = $_POST['id'];

     $data = SectionData::getAllBySubsection($section);
}
if ($_POST['opt'] == 'period_id') {

     $id_period = $_POST['id'];

     $data = SectionData::getPeriodById($id_period);
}
if ($_POST['opt'] == 'element') {

     $id_element = $_POST['id'];

     $data = ElementData::getAllByPeriod($id_element);
}
if ($_POST['opt'] == 'element_id') {

     $id_element = $_POST['id'];

     $data = ElementData::getElementById($id_element);
}

echo json_encode($data);
