<?php
echo '<pre>';
print_r($_POST);
echo '</pre>';

if ($_POST['type'] == 1) {

	$entity = new NewsData();

	foreach ($_POST as $k => $v) {
		$entity->$k = $v;
	}

	/* if (!isset($_POST["title"])) {
		$entity->title = '';
	}
	if (!isset($_POST["link"])) {
		$entity->link = '';
	}
	if (!isset($_POST["link"])) {
		$entity->link = '';
	} */

	if ($_FILES['cover_page']['error'] == 0) {
		$handle = new Upload($_FILES['cover_page']);
		if ($handle->uploaded) {
			$url = "./files/imgNews/";
			$handle->Process($url);
			$cover_page = $handle->file_dst_name;
		}
		$entity->cover_page = $cover_page;
	} else {
		$entity->cover_page = '';
	}

	$files = array_filter($_FILES['multimedia']['name']);
	$total_count = count($_FILES['multimedia']['name']);
	for ($i = 0; $i < $total_count; $i++) {
		$tmpFilePath = $_FILES['multimedia']['tmp_name'][$i];
		if ($tmpFilePath != "") {
			$newFilePath = "./files/imgNews/" . $_FILES['multimedia']['name'][$i];
			if (move_uploaded_file($tmpFilePath, $newFilePath)) {
			}
		}
	}
	$entity->multimedia = implode(',',  $_FILES['multimedia']['name']);

	if ($_FILES['video']['error'] == 0) {
		$handle = new Upload($_FILES['video']);
		if ($handle->uploaded) {
			$url = "./files/videosNews/";
			$handle->Process($url);
			$video = $handle->file_dst_name;
		}
		$entity->video = $video;
	} else {
		$entity->video = '';
	}

	$entity->id = $_POST['id'];
	/* $entity->description = $_POST['description']; */
	/* $entity->update_by = $_SESSION["user_id"]; */
	$entity->is_active = 1;
	/* $entity->departments = isset($_POST['departments']) == true ? $_POST['departments'] : 0; */

	if (!isset($_POST["departments"])) {
		$entity->departments = '';
	}
	
	$entity->updateAPI();
} else {

	$entity = new NewsData();

	foreach ($_POST as $k => $v) {
		$entity->$k = $v;
	}
	$entity = new NewsData();

	foreach ($_POST as $k => $v) {
		$entity->$k = $v;
	}
	if (!isset($_POST["link"])) {
		$entity->link = '';
	}
	if ($_FILES['cover_page']['error'] == 0) {
		$handle = new Upload($_FILES['cover_page']);
		if ($handle->uploaded) {
			$url = "./files/imgNewsWeb/";
			$handle->Process($url);
			$cover_page = $handle->file_dst_name;
		}
		$entity->cover_page = $cover_page;
	} else {
		$entity->cover_page = '';
	}
	$files = array_filter($_FILES['multimedia']['name']);

	$total_count = count($_FILES['multimedia']['name']);
	for ($i = 0; $i < $total_count; $i++) {
		$tmpFilePath = $_FILES['multimedia']['tmp_name'][$i];
		if ($tmpFilePath != "") {
			$newFilePath = "./files/imgNewsWeb/" . $_FILES['multimedia']['name'][$i];
			if (move_uploaded_file($tmpFilePath, $newFilePath)) {
			}
		}
	}
	$entity->multimedia = implode(',',  $_FILES['multimedia']['name']);

	if ($_FILES['video']['error'] == 0) {
		$handle = new Upload($_FILES['video']);
		if ($handle->uploaded) {
			$url = "./files/videoNewsWeb/";
			$handle->Process($url);
			$video = $handle->file_dst_name;
		}
		$entity->video = $video;
	} else {
		$entity->video = '';
	}

	$entity->id = $_POST['id'];
	$entity->description = $_POST['ckeditor'];
	$entity->update_by = $_SESSION["user_id"];
	$entity->updateAPI();
}
