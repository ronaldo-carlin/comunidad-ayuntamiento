<?php
/* echo '<pre>';
print_r($_POST);
echo '</pre>'; */

$entity =  new UserData();
foreach ($_POST as $k => $v) {
	$entity->$k = $v;
	# code...
}

if ($_FILES['img']['error'] == 0) {
	$handle = new Upload($_FILES['img']);
	if ($handle->uploaded) {
		$url = "./files/imgUsers/";
		$handle->Process($url);
		$img = $handle->file_dst_name;
	}
	$entity->img = $img;
} else {
	$entity->img = '';
}

if (!isset($_POST["department_id"])) {
	$entity->department_id = '';
}

if (!isset($_POST["area_id"])) {
	$entity->area_id = '';
}

$entity->id = $_POST['id'];

$entity->update_by = $_SESSION["user_id"];
$entity->role_id = $_SESSION["rol_id"];

$entity->editAPI();
Core::redir("./index.php?view=usuarios");