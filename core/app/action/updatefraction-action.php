<?php
echo '<pre>';
print_r($_POST);
echo '</pre>';

$entity =  new FractionData();

foreach ($_POST as $k => $v) {
	$entity->$k = $v;
}

if ($_FILES['filename']['error'] == 0) {
	$handle = new Upload($_FILES['filename']);
	if ($handle->uploaded) {
		$url = "./files/iconFraction/";
		$handle->Process($url);
		$filename = $handle->file_dst_name;
	}
	$entity->filename = $filename;
} else {
	$entity->filename = '';
}

$entity->id = $_POST['id'];

if (!isset($_POST["department_id"])) {
	$entity->department_id = '';
}

if (!isset($_POST["type_id"])) {
	$entity->type_id  = '';
}

if (!isset($_POST["update_period"])) {
	$entity->update_period = '';
}

$entity->updated_by = $_SESSION["user_id"];

/* echo '<pre>';
print_r($entity);
echo '</pre>'; */

$entity->updateAPI();