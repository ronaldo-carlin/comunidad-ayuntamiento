<?php
/* echo '<pre>';
print_r($_POST);
echo '</pre>'; */


$entity =  new UserData();
foreach ($_POST as $k => $v) {
    $entity->$k = $v;
    # code...
}

$handle = new Upload($_FILES['img']);
if ($handle->uploaded) {
    $url = "files/imgUsers/";
    $handle->Process($url);

    $entity->img = $handle->file_dst_name;
}

$entity->created_by = $_SESSION["user_id"];
$entity->role_id = $_SESSION["rol_id"];
$entity->administration_id = $_SESSION["administration_id"];

/* echo '<pre>';
print_r($entity);
echo '</pre>'; */

$entity->addAPI();
Core::redir("./index.php?view=usuarios");