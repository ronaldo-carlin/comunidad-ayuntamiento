<?php
class UserData
{
	public static $tablename = "users";

	public function __construct()
	{
	}
	public function add()
	{
		$sql = "insert into " . self::$tablename . " (name,email,password,phone,enrollment,curp,rfc,imss,department,role,address,date_birth,date_admission,company_id,created_at,created_by,role_id,area,img)";
		$sql .= "value (\"$this->name\",\"$this->email\",\"$this->password\",\"$this->phone\",\"$this->enrollment\",\"$this->curp\",\"$this->rfc\",\"$this->imss\",\"$this->department\",\"$this->role\",\"$this->address\",\"$this->date_birth\",\"$this->date_admission\",\"$this->company_id\",NOW(),$this->created_by,$this->role_id,\"$this->area\",\"$this->img\")";
		//echo $sql;
		return Executor::doit($sql);
	}

	public static function delById($id)
	{
		$sql = "update " . self::$tablename . " set deleted=1, deleted_at=NOW() where id=$id";
		Executor::doit($sql);
	}
	public function del()
	{
		$sql = "delete from " . self::$tablename . " where id=$this->id";
		//$sql = "update ".self::$tablename." set deleted=1, deleted_at=NOW(), deleted_by=$this->user_id where id=$this->id";
		Executor::doit($sql);
	}

	// partiendo de que ya tenemos creado un objecto UserData previamente utilizamos el contexto
	public function update()
	{
		$sql = "update " . self::$tablename . " set name=\"$this->name\", email=\"$this->email\",phone=\"$this->phone\",enrollment=\"$this->enrollment\",curp=\"$this->curp\",rfc=\"$this->rfc\",imss=\"$this->imss\",area=\"$this->area\",department=\"$this->department\",role=\"$this->role\", address=\"$this->address\",date_birth=\"$this->date_birth\",date_admission=\"$this->date_admission\",company_id=\"$this->company_id\",role_id=\"$this->role_id\",modified_at=NOW(),modified_by=\"$this->modified_by\" where id=$this->id";
		//echo $sql;
		return Executor::doit($sql);
	}
	public static function updateSalary($id, $salary_day, $salary_month, $bonus_day, $days)
	{
		$sql = "update " . self::$tablename . " set salary_day=$salary_day,salary_month=$salary_month, bonus_day=$bonus_day, days=$days  where id=$id";
		Executor::doit($sql);
	}

	public function updateProfile()
	{
		$sql = "update " . self::$tablename . " set name=\"$this->name\",lastname=\"$this->lastname\",phone=\"$this->phone\",email=\"$this->email\",payroll_number=\"$this->payroll_number\",rfc=\"$this->rfc\", is_manager=\"$this->is_manager\", modified_at=NOW(), modified_by=\"$this->user_id\" where id=$this->id";
		Executor::doit($sql);
	}

	public static function update_passwd($id, $password)
	{
		$sql = "update " . self::$tablename . " set password=\"$password\" where id=$id";
		//echo $sql;
		Executor::doit($sql);
	}

	public static function getById($id)
	{
		$sql = "select * from " . self::$tablename . " where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new UserData());
	}

	public static function getByMail($mail)
	{
		$sql = "select * from " . self::$tablename . " where email=\"$mail\"";
		$query = Executor::doit($sql);
		return Model::one($query[0], new UserData());
	}
	public static function getAllByUserId($id)
	{
		$sql = "select * from " . self::$tablename . " where created_by = $id order by name asc";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}
	public static function getAll()
	{
		$sql = "select * from " . self::$tablename . " where deleted=0";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}
	public static function getCount()
	{
		$sql = "select COUNT(ID) as count from " . self::$tablename;
		$query = Executor::doit($sql);
		return Model::one($query[0], new UserData());
	}
	public static function getByPositionId($id)
	{
		$sql = "select * from " . self::$tablename . " where position_id = $id";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}
	public  static function getByDateBirth()
	{
		$sql = "select * from " . self::$tablename . "  where  day(date_birth)=day(CURDATE()) and MONTH(date_birth)=MONTH(CURDATE())";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}

	//* Crea el usuario
	public  function addAPI()
	{

		$url = $GLOBALS["api"] . '/createUser?role_id=' . $this->role_id;

		$data = [
			'name' => $this->name,
			'email' => $this->email,
			'password' => $this->password,
			'date_birth' =>   $this->date_birth,
			'phone' => $this->phone,
			'img' => $this->img,
			'address' => $this->address,
			'curp' => $this->curp,
			'rfc' => $this->rfc,
			'imss' => $this->imss,
			'date_admission' =>  $this->date_admission,
			'department_id' => $this->department_id,
			'area_id' => $this->area_id,
			'role_id' => $this->role_id,
			'role' => $this->role,
			'created_by' => $this->created_by,

		];
		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		//echo $result;
	}

	//* Actualiza el usuario
	public  function editAPI()
	{

		$url = $GLOBALS["api"] . '/updateUser?id=' . $this->id;

		$data = [

			'name' => $this->name,
			'email' => $this->email,
			'date_birth' =>   $this->date_birth,
			'phone' => $this->phone,
			'img' => $this->img,
			'address' => $this->address,
			'curp' => $this->curp,
			'rfc' => $this->rfc,
			'imss' => $this->imss,
			'date_admission' =>  $this->date_admission,
			'department_id' => $this->department_id,
			'area_id' => $this->area_id,
			'role_id' => $this->role_id,
			'role' => $this->role,
			'update_by' => $this->update_by,

		];
		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		//echo $result;
	}

	//* Borra el usuario
	public static function deletedAPI($id, $role_id, $user)
	{
		$url = $GLOBALS["api"] . '/deleteUser?id=' . $id . '&role_id=' . $role_id . '&user=' . $user;

		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		//echo $result;
	}

	//* Actualiza el perfil
	public  function updateProfileAPI()
	{

		$url = $GLOBALS["api"] . '/updateProfile?api_token=' . $this->api_token . '&id=' . $this->id;

		$data = [
			'name' => $this->name,
			'email' => $this->email,
			'phone' => $this->phone,
			'curp' => $this->curp,
			'address' => $this->address,
			'deleted_by' => $this->deleted_by,

		];
		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);
	}

	//* Actualiza el password
	public  function updatePassAPI()
	{

		$url = $GLOBALS["api"] . '/updatePassword?id=' . $this->id;

		if ($this->first  == 1) {
			$data = [
				'password' => $this->password,
				'first' => $this->first,
				/* 'img' => $this->img, */

			];
		} else {
			$data = [
				'password' => $this->password,
				'first' => $this->first,

			];
		}

		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);
	}

	//* Trae los usuarios activos
	public static function loginAPI($email, $password)
	{
		$url = $GLOBALS["api"] . '/login?email=' . $email . '&password=' . $password;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	//* Olvidaste tu contraseña
	public static function forgotpassword($email)
	{

		$url = $GLOBALS["api"] . '/forgotpassword';

		$data = [
			'email' => $email,

		];
		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result, true);
	}

	/* public static function loginAPI($email, $password)
	{

		$url = $GLOBALS["api"] . '/login';

		$data = [
			'email' => $email,
			'password' => $password,

		];
		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result, true);
		
	}
 */
	//* Trae los usuarios activos
	public static function getUsersActivesAPI($role_id)
	{
		$url = $GLOBALS["api"] . '/getUsersActives?role_id=' . $role_id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	//* Trae el usuario por id
	public static function getUserByIdAPI($id)
	{

		$url = $GLOBALS["api"] . '/getUserByIdAPI?id=' . $id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	//* Trae los usuarios por compañia
	public static function getUsersByCompanyAPI($company_id)
	{

		$url = $GLOBALS["api"] . '/getUsersByCompany?company_id=' . $company_id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	//* Trae los ultimos 3 cumpleaños y los proximos 3 cumpleaños por compañia
	public static function getBirthdaysAPI($company_id)
	{

		$url = $GLOBALS["api"] . '/getBirthdays?company_id=' . $company_id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	//* Trae los los recibos
	public static function getInvoice($enrollment)
	{

		$url = $GLOBALS["api"] . '/getInvoice?enrollment=' . $enrollment;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	//* Trae el pdf
	public static function getArchive($id)
	{

		$url = $GLOBALS["api"] . '/getArchive?id=' . $id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	public static function getEvalByCompanyIdAndCodeId($company_id, $code_id)
	{
		$sql = "select * from " . self::$tablename . " where company_id =$company_id and position_id = $code_id";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}

	public static function showCova($id, $show)
	{
		$sql = "update " . self::$tablename . " set showConva=$show where id=$id";
		Executor::doit($sql);
	}

	public static function getLike($q)
	{
		$sql = "select * from " . self::$tablename . " where name like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UserData());
	}

	public static function getPermissonsByUserAndModule($id, $module_id)
	{
		$sql = "select u.rol_id AS 'role', P.modules_id AS 'modulos', P.created AS 'create' ,P.read AS 'read', P.edit AS 'edit', P.deleted AS 'delete' from " . self::$tablename . ' u' . " INNER JOIN  permissions P ON P.role_id = u.rol_id where u.id = $id and P.modules_id = $module_id";
		//echo $sql;
		$query = Executor::doit($sql);
		return Model::one($query[0], new PermissionsData());
	}

	public static function getPermissonsByUserAndModuleApi($id, $module_id)
	{
		$url = $GLOBALS["api"] . '/getPermissonsByUserAndModuleApi?id=' . $id . '&module_id=' . $module_id;
		//*   echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
}
