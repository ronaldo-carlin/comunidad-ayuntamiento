<?php

class DepartmentsData
{
	public static $tabletitle = "departments";

	public function __construct()
	{
	}
	
	// * Trae todos los departamentos
	public static function getDepartments()
	{
		$url = $GLOBALS["api"] . '/getDepartments';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	
}
