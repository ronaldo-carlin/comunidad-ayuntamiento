<?php

class TransparencyData
{
	public static $tabletitle = "transparency";

	public function __construct()
	{
	}

	//* Crea la transparencia
	public  function addAPI()
	{

		$url = $GLOBALS["api"] . '/createNew';
		$data = [
			'title' => $this->title,
			'description' => $this->description,
			'cover_page' => $this->cover_page,
			'multimedia' => $this->multimedia,
			'video' => $this->video,
			'link' => $this->link,
			'link_youtube' => $this->link_youtube,
			'type' => $this->type,
			'departments' => $this->departments,
			'publish' => $this->publish,
			'date_validation' =>  $this->date_validation,
			'created_by' =>   $this->created_by,
		];

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}

	//* Actualiza la transparencia
	public  function updateAPI()
	{

		$url = $GLOBALS["api"] . '/updateNew?id=' . $this->id;

			$data = [
				
				'title' => $this->title,
				'description' => $this->description,
				'cover_page' => $this->cover_page,
				'multimedia' => $this->multimedia,
				'video' => $this->video,
				'link' => $this->link,
				'link_youtube' => $this->link_youtube,
				'departments' => $this->departments,
				'type' => $this->type,
				'is_active' => $this->is_active,
				'publish' => $this->publish,
				'date_validation' =>  $this->date_validation,
				'update_by' =>   $this->update_by,

			];

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}

	//* Borra la transparencia
	public static function deletedAPI($id)
	{
		$url = $GLOBALS["api"] . '/deleteNew?id=' . $id;

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}

	//* Publica la transparencia
	public static function publishAPI($id)
	{
		$url = $GLOBALS["api"] . '/publishNew?id=' . $id;

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		/* echo $result; */
	}

	//* Despublica la transparencia
	public static function unpublishAPI($id)
	{
		$url = $GLOBALS["api"] . '/unpublishNew?id=' . $id;

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		/* echo $result; */
	}

	// * Trae todas las transparencias activas
	public static function getActivesAPI()
	{
		$url = $GLOBALS["api"] . '/getNewsActives';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae todas las transparencias
	public static function getAllTransparency()
	{
		$url = $GLOBALS["api"] . '/getAllTransparency';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae la transparencia por id
	public static function getTransparencyById($id)
	{

		$url = $GLOBALS["api"] . '/getTransparencyById?id=' . $id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

}
