<?php

class PromotionsData
{
	public static $tablename = "promotions";


	public function __construct()
	{

		//Van todos los campos que tienes en tu tabla

		$this->name = "";
		$this->description = "";
		$this->company_id = "";
		$this->created_at = "";
		$this->created_by = "";
		$this->modified_at = "";
		$this->modified_by = "NOW()";
		$this->deleted_at = "";
		$this->deleted_by = "NOW()";
		$this->deleted = "";
	}

	public function add()
	{
		$sql = "insert into " . self::$tablename . " (name, description, date_validity, cover_page, publish, company_id ,created_at,created_by,companies_avalibles,multimedia) ";
		$sql .= "value (\"$this->name\", \"$this->description\", \"$this->date_validity\",\"$this->cover_page\",\"$this->publish\",\"$this->company_id\",NOW(),$this->user_id,\"$this->companies_avalibles\",\"$this->multimedia\")";
		//echo $sql;
		Executor::doit($sql);
	}

	public static function delById($id)
	{
		$sql = "delete from " . self::$tablename . " where id=$id";
		Executor::doit($sql);
	}
	public static function  del($id)
	{
		//$sql = "delete from ".self::$tablename." where id=$this->id";
		$sql = "update " . self::$tablename . " set deleted=1, delete_at=NOW() where id=$id";
		Executor::doit($sql);
	}
	public static function unpublish($id)
	{
		$sql = "update " . self::$tablename . " set publish=0 where id=$id";
		//echo $sql;
		$query = Executor::doit($sql);
	}

	public function update()
	{
		$sql = "update " . self::$tablename . " set name=\"$this->name\",company_id=\"$this->company_id\",description=\"$this->description\", date_validity=\"$this->date_validity\",publish=\"$this->publish\",companies_avalibles=\"$this->companies_avalibles\", update_at=NOW(), update_by=$this->update_by where id=$this->id";
		//echo $sql;
		return Executor::doit($sql);
	}

	public static function publish($id)
	{
		$sql = "update " . self::$tablename . " set publish=1 where id=$id";
		//echo $sql;
		$query = Executor::doit($sql);
	}


	//* Crea la promocion
	public  function addAPI()
	{

		$url = $GLOBALS["api"] . '/createPromotion?api_token='.$this->api_token;

		$data = [
			'name' => $this->name,
			'description' => $this->description,
			'cover_page' => $this->cover_page,
			'multimedia' => $this->multimedia,
			'date_validity' => $this->date_validity,
			'created_by' =>   $this->created_by,
			'company_id' => $this->company_id,
			'publish' => $this->publish,
			'companies_avalibles' => $this->companies_avalibles,

		];
		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Actualiza la promocion
	public  function updateAPI()
	{

		$url = $GLOBALS["api"] . '/updatePromotion?api_token='.$this->api_token.'&id=' . $this->id;

		$data = [
			'name' => $this->name,
			'description' => $this->description,
			'cover_page' => $this->cover_page,
			'multimedia' => $this->multimedia,
			'date_validity' =>  $this->date_validity,
			'update_by' =>   $this->update_by,
			'company_id' => $this->company_id,
			'publish' => $this->publish,
			'companies_avalibles' => $this->companies_avalibles,

		];


		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Borra el promocion
	public static function deletedAPI($id,$api_token)
	{
		$url = $GLOBALS["api"] . '/deletePromotion?api_token='.$api_token.'&id=' . $id;

		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Publica el promocion
	public static function publishAPI($id,$api_token)
	{
		$url = $GLOBALS["api"] . '/publishPromotion?api_token='.$api_token.'&id=' . $id;

		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Despublica el promocion
	public static function unpublishAPI($id,$api_token)
	{
		$url = $GLOBALS["api"] . '/unpublishPromotion?api_token='.$api_token.'&id=' . $id;

		$headers = [
			'Content-Type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}
	//* Trae las promociones activas
	public static function getPromotionsActivesAPI($api_token)
	{

		$url = $GLOBALS["api"] . '/getPromotionsActives?api_token='.$api_token.'';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	//* Trae las promociones por compañia
	public static function getLikeCompaniesAvalibleAPI($company_id,$api_token)
	{

		$url = $GLOBALS["api"] . '/getPromotionsLikeCompaniesAvalible?api_token='.$api_token.'&company_id=' . $company_id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	//* Trae las promociones por id
	public static function getByIdAPI($id,$api_token)
	{

		$url = $GLOBALS["api"] . '/getPromotionsById?api_token='.$api_token.'&id=' . $id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	public static function getById($id)
	{
		$sql = "select * from " . self::$tablename . " where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new PromotionsData());
	}
	public static function getByCoporativo()
	{
		$sql = "select * from " . self::$tablename . " where company_id=2 and publish=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new PromotionsData());
	}
	public static function getBySuperBoletos()
	{
		$sql = "select * from " . self::$tablename . " where company_id=3 and publish=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new PromotionsData());
	}
	public static function getPublish()
	{
		$sql = "select * from " . self::$tablename . " where publish = 1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new PromotionsData());
	}
	public static function getAllCount()
	{
		$sql = "select count(id) as count from " . self::$tablename . " where  deleted=0";
		$query = Executor::doit($sql);
		return Model::one($query[0], new ReleaseData());
	}
	public static function getPublishCount()
	{
		$sql = "select count(id) as count from " . self::$tablename . " where publish =1 and deleted=0 ";
		$query = Executor::doit($sql);
		return Model::one($query[0], new ReleaseData());
	}
	public static function getNopublishCount()
	{
		$sql = "select count(id) as count from " . self::$tablename . " where publish =0  and deleted=0";
		$query = Executor::doit($sql);
		return Model::one($query[0], new ReleaseData());
	}
	public static function getAll()
	{
		$sql = "select * from " . self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0], new PromotionsData());
	}

	public static function getActives()
	{
		$sql = "select * from " . self::$tablename . " where deleted = 0 ";
		$query = Executor::doit($sql);

		return Model::many($query[0], new PromotionsData());
	}

	//* Funciones viejas

	/*public static function getLikeCompaniesAvalible($id)
	{
		$sql = "select * from " . self::$tablename . " where companies_avalibles LIKE  \"%$id%\"  and publish = 1 and deleted=0";
		//echo $sql;
		$query = Executor::doit($sql);
		return Model::many($query[0], new ReleaseData());
	}*/
	/* public static function getByCNCI()
	{
		$sql = "select * from " . self::$tablename . " where company_id=1 and publish=1";
		//echo $sql;
		$query = Executor::doit($sql);
		return Model::many($query[0], new PromotionsData());
	} */
}
