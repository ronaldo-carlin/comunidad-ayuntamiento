<?php

class PermissionsData
{
    public static $tablename = "permissions";



	public function __construct(){

		//Van todos los campos que tienes en tu tabla

	

	}

	public function add(){
		$sql = "insert into ".self::$tablename." (name,created_at,created_by,is_active) ";
		$sql .= "value (\"$this->name\", NOW(),$this->user_id,\"$this->is_active\")";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}

	public static function  del($id){
		//$sql = "delete from ".self::$tablename." where id=$this->id";
	    $sql = "update ".self::$tablename." set deleted=1, deleted_at=NOW() where id=$id";
		Executor::doit($sql);
	}
   
	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\",modified_at=NOW(), modified_by=$this->user_id where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new PermissionsData());

	}

	public static function getByRoleId($id){
		$sql = "select * from ".self::$tablename." where role_id=$id";
		$query = Executor::doit($sql);
		return Model::many($query[0],new PermissionsData());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new PermissionsData());
	}

	public static function getActives(){
		$sql = "select * from ".self::$tablename." where deleted = 0";
		$query = Executor::doit($sql);
		return Model::many($query[0],new PermissionsData());
	}

	public static function getModuleAdminByRole($id){
		$sql = "select   r.name AS 'role', m.id as 'module_id', m.name AS 'module', m.ahref AS 'ahref', m.parent_id AS 'parent' from ".self::$tablename. ' s'."  INNER JOIN roles r  ON s.role_id = r.id  INNER JOIN modules m  on s.module_id = m.id where s.role_id =$id and m.parent_id IS NULL ";
		//echo $sql . '<br>';
		$query = Executor::doit($sql);
		return Model::one($query[0],new PermissionsData());
	}

	public static function getSubmoduleByRole($id, $parent_id){
		$sql = "select r.name AS 'role', m.name AS 'module', m.ahref AS 'ahref',  m.parent_id AS 'parent' from  ".self::$tablename. ' s'."  INNER JOIN roles r  ON s.role_id = r.id  INNER JOIN modules m  on s.module_id = m.id where s.role_id = $id and parent_id= $parent_id and m.is_active =1";
		//echo $sql . '<br>';
		$query = Executor::doit($sql);
		return Model::many($query[0],new PermissionsData());
	}
	
	public static function getPermissonsByUserAndModule($id, $module_id){
		$sql = "select u.role_id AS 'role', s.create AS 'create' ,s.read AS 'read', s.edit AS 'edit', s.delete AS 'delete' from ".self::$tablename. ' u' ."  INNER JOIN  permissions s ON s.role_id = u.role_id  where u.id =$id and s.module_id=$module_id";
		//echo $sql;
		$query = Executor::doit($sql);
		return Model::one($query[0],new PermissionsData());
	}
}