<?php

class RolesData
{
    public static $tablename = "roles";



	public function __construct(){

		//Van todos los campos que tienes en tu tabla

		$this->name = "";
		$this->created_at = "";
		$this->created_by = "";
		$this->modified_at = "";
		$this->modified_by = "NOW()";
		$this->deleted_at = "";
		$this->deleted_by = "NOW()";
		$this->deleted= "";

	}

	public function add(){
		$sql = "insert into ".self::$tablename." (name,created_at,created_by,is_active) ";
		$sql .= "value (\"$this->name\", NOW(),$this->user_id,\"$this->is_active\")";
		Executor::doit($sql);
	}

	public static function delById($id){
		$sql = "delete from ".self::$tablename." where id=$id";
		Executor::doit($sql);
	}

	public static function  del($id){
		//$sql = "delete from ".self::$tablename." where id=$this->id";
	    $sql = "update ".self::$tablename." set deleted=1, deleted_at=NOW() where id=$id";
		Executor::doit($sql);
	}
   
	public function update(){
		$sql = "update ".self::$tablename." set name=\"$this->name\",modified_at=NOW(), modified_by=$this->user_id where id=$this->id";
		Executor::doit($sql);
	}

	public static function getById($id){
		$sql = "select * from ".self::$tablename." where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0],new RolesData());

	}

	public static function getAll(){
		$sql = "select * from ".self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0],new RolesData());
	}

	public static function getActives(){
		$sql = "select * from ".self::$tablename." where deleted = 0";
		$query = Executor::doit($sql);
		return Model::many($query[0],new RolesData());
	}

	public static function getActivesAPI()
	{
		$url = $GLOBALS["api"] . '/getRolesActives';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

}