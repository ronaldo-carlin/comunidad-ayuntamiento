<?php

class FractionData
{
	public static $tabletitle = "fractions";

	public function __construct()
	{
	}

	//* Crea la fraccion
	public  function addAPI()
	{

		$url = $GLOBALS["api"] . '/createFraction?role_id=' . $this->role_id;
		$data = [
			'filename' => $this->filename,
			'fraction' => $this->fraction,
			'name' => $this->name,
			'description' => $this->description,
			'format' => $this->format,
			'update_date' => $this->update_date,
			'date_validation' => $this->date_validation,
			'type_id' => $this->type_id,
			'department_id' => $this->department_id,
			'administration_id' => $this->administration_id,
			'update_period' => $this->update_period,
			'created_by' =>   $this->created_by,
		];

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}

	//* Actualiza la fraccion
	public  function updateAPI()
	{

		$url = $GLOBALS["api"] . '/updateFraction?id=' . $this->id;

		$data = [

			'filename' => $this->filename,
			'fraction' => $this->fraction,
			'name' => $this->name,
			'description' => $this->description,
			'format' => $this->format,
			'update_date' => $this->update_date,
			'date_validation' => $this->date_validation,
			'type_id' => $this->type_id,
			'department_id' => $this->department_id,
			'update_period' => $this->update_period,
			'updated_by' =>   $this->updated_by,

		];

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}

	//* Borra la fraccion
	public static function deletedAPI($id)
	{
		$url = $GLOBALS["api"] . '/deleteFraction?id=' . $id;

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}

	// * Trae todas las fraccions activas
	public static function getActivesAPI()
	{
		$url = $GLOBALS["api"] . '/getNewsActives';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae todas las fraccions
	public static function getAllFraction()
	{
		$url = $GLOBALS["api"] . '/getAllFraction';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae la fraccion por administracion
	public static function getAllByAdmin($admin)
	{

		$url = $GLOBALS["api"] . '/getAllByAdmin?admin=' . $admin;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae la fraccion por administracion
	public static function getAllByAdminandDepartment($admin, $department_id)
	{

		$url = $GLOBALS["api"] . '/getAllByAdminandDepartment?admin=' . $admin . '&department_id=' . $department_id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae la fraccion por id
	public static function getFractionById($id)
	{

		$url = $GLOBALS["api"] . '/getFractionById?id=' . $id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae todos los tipos de fraccion
	public static function getTypeFraction()
	{

		$url = $GLOBALS["api"] . '/getTypeFraction';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae todos los periodos
	public static function getAllPeriod()
	{

		$url = $GLOBALS["api"] . '/getAllPeriod';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
}
