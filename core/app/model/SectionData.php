<?php

class SectionData
{
	public static $tabletitle = "section";

	public function __construct()
	{
	}

	//* Crea la seccion
	public  function addAPI()
	{

		$url = $GLOBALS["api"] . '/createSection?role_id=' . $this->role_id;

		if ($this->type == 1) {
			$data = [
				'fraction_id' => $this->fraction_id,
				'name' => $this->name,
				'description' => $this->description,
				'created_by' =>   $this->created_by,
				'type' =>   $this->type,
			];
		} else {
			$data = [
				'subsection' => $this->subsection,
				'name' => $this->name,
				'description' => $this->description,
				'created_by' =>   $this->created_by,
				'type' =>   $this->type,
			];
		}

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		//echo $result;
	}

	//* Actualiza la seccion
	public  function updateAPI()
	{

		$url = $GLOBALS["api"] . '/updateSection';

			$data = [

				'id' => $this->id,
				'name' => $this->name,
				'description' => $this->description,
				'updated_by' =>   $this->updated_by,

			];

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}

	//* Borra la seccion
	public static function deletedAPI($id)
	{
		$url = $GLOBALS["api"] . '/deleteSection?id=' . $id;

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		//echo $result;
	}

	// * Trae todas las secciones
	public static function getAllSection()
	{
		$url = $GLOBALS["api"] . '/getAllSection';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae la seccion por fraccion
	public static function getAllByFraction($fraction)
	{

		$url = $GLOBALS["api"] . '/getAllByFraction?fraction=' . $fraction;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae el periodo por seccion
	public static function getAllBySubsection($section)
	{

		$url = $GLOBALS["api"] . '/getAllBySubsection?section=' . $section;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae la seccion por id
	public static function getSectionById($id)
	{

		$url = $GLOBALS["api"] . '/getSectionById?id=' . $id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae la seccion por id
	public static function getPeriodById($id)
	{

		$url = $GLOBALS["api"] . '/getPeriodById?id=' . $id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae todos los tipos de seccion
	public static function getTypeSection()
	{

		$url = $GLOBALS["api"] . '/getTypeSection';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
}
