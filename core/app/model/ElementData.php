<?php

class ElementData
{
	public static $tabletitle = "section";

	public function __construct()
	{
	}

	//* Crea la seccion
	public  function addAPI()
	{

		$url = $GLOBALS["api"] . '/createElement?role_id=' . $this->role_id;

		$data = [
			'section_id' => $this->section_id,
			'link' => $this->link,
			'description' => $this->description,
			'created_by' =>   $this->created_by,
		];

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		//echo $result;
	}

	//* Actualiza la seccion
	public  function updateAPI()
	{

		$url = $GLOBALS["api"] . '/updateElement?id=' . $this->id;

		$data = [

			'id' => $this->id,
			'link' => $this->link,
			'description' => $this->description,
			'updated_by' =>   $this->updated_by,

		];

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		echo $result;
	}

	//* Borra la seccion
	public static function deletedAPI($id)
	{
		$url = $GLOBALS["api"] . '/deleteElement?id=' . $id;

		$headers = [
			'Content-type: application/json'
		];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		$result = curl_exec($ch);
		curl_close($ch);

		//echo $result;
	}

	// * Trae todas los elementos disponibles
	public static function getAllElement()
	{
		$url = $GLOBALS["api"] . '/getAllElement';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae el elemento por id
	public static function getElementById($id)
	{

		$url = $GLOBALS["api"] . '/getElementById?id=' . $id;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}

	// * Trae los elementos del periodo
	public static function getAllByPeriod($subsection)
	{

		$url = $GLOBALS["api"] . '/getAllByPeriod?section_id=' . $subsection;
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
}
