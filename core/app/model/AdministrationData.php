<?php

class administrationData
{
	public static $tabletitle = "administration";

	public function __construct()
	{
	}
	
	// * Trae todos los departamentos
	public static function getAllAdministration()
	{
		$url = $GLOBALS["api"] . '/getAllAdministration';
		//echo $url;
		$json = file_get_contents($url);
		return json_decode($json, true);
	}
	
}
